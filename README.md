<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示</font></font></h1><a id="user-content-decryptprompt" class="anchor" aria-label="永久链接：解密提示" href="#decryptprompt"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果LLM的突然到来让你感到沮丧，认知读下主目录的抑郁症AI学者选择你的武器生存策略持续更新以下内容，星标持续更新~</font></font></p>
</blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录顺序如下</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">三维，垂直领域大模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Agent和指令调用等训练框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开源指令，预训练，rlhf，对话，代理训练数据整理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AIGC相关应用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示写作指南和5星博客等资源整理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prompt和LLM论文解读方向梳理</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我的博客</font></font></h2><a id="user-content-my-blogs" class="anchor" aria-label="永久链接：我的博客" href="#my-blogs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://cloud.tencent.com/developer/article/2215545?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列1.免调提示：GPT2 &amp; GPT3 &amp; LAMA &amp; AutoPrompt</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2223355?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列2.冻结提示扭矩LM： T5 &amp; PET &amp; LM-BFF</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2237259?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列3.冻结 LM Prompt: Prefix-tuning &amp; Prompt-tuning &amp; P-tuning</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2245094?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列4.升级指令调优：Flan/T0/InstructGPT/TKInstruct</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2260697?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列5. APE+SELF=自动化指令集构建代码实现</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2276508" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列6. lora指令指标扣细节-请安静，1个小时真不够~</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/old/2289566?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列7.偏好分析RLHF-OpenAI·DeepMind·Anthropic对比分析</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/old/2295783?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列8.消耗让训练LLM支持超长输入：知识库 &amp; Unlimiformer &amp; PCW &amp; NBCE</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/old/2296079?areaSource=&amp;traceId=" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列9.模型复杂推理-思维链基础和进阶玩法</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/old/2298660" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列10.链COT思维原理研究</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/old/2301999" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列11.小模型也能COT，先天不足后天补</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2305421" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列12. LLM代理零范式ReAct &amp; Self Ask</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2312674" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列13. LLM Agent指令参数方案: Toolformer &amp; Gorilla</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2319879" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列14. LLM Agent之搜索应用设&ZeroWidthSpace;&ZeroWidthSpace;计：WebGPT &amp; WebGLM &amp; WebCPM</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2328749" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列15. LLM Agent之数据库应用设计：DIN &amp; C3 &amp; SQL-Palm &amp; BIRD</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2333495" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列16. LLM 执业经验之数据越少越好？LTD &amp; LIMA &amp; AlpaGasus</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2338592" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列17. LLM翻译方案再升级WizardLM &amp; BackTranslation &amp; SELF-ALIGN</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2351540" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列18. LLM Agent之只有智能体的世界</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2358413" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列19. LLM Agent之数据分析领域的应用：Data-Copilot &amp; InsightPilot</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2365050" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列20. LLM Agent之再谈RAG的回调优化优化</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2369977" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列21. LLM Agent之再谈RAG的认知信息密度和质量</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2375066" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">&ZeroWidthSpace;解密提示系列22. LLM Agent之RAG的反思：放弃了压缩还是智能么？</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2378383" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prompt系列23.大模型幻觉分类&amp;定位&amp;检测&amp;缓解方案脑图解密全整理</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2389619" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列24。 RLHF新方案之训练策略：SLiC-HF &amp; DPO &amp; RRHF &amp; RSO</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2398654" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列25。 RLHF改良方案之样本标签：RLAIF &amp; SALMON</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2394120" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列26。人类思考vs模型思考：抽象与发散思维</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2406888" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列27. LLM翻译经验之如何降低通用能力损失</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2411792" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列28. LLM Agent之金融领域智能体：FinMem &amp; FinAgent</font></font></a></li>
<li><a href="https://cloud.tencent.com/developer/article/2415908" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解密提示系列29. LLM Agent之真实世界海量API解决方案：ToolLLM &amp; AnyTool</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLMS</font></font></h2><a id="user-content-llms" class="anchor" aria-label="永久链接：LLMS" href="#llms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型体育</font></font></h3><a id="user-content-模型评测" class="anchor" aria-label="永久链接： 模型足球" href="#模型评测"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">名单</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">结果</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://tatsu-lab.github.io/alpaca_eval/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlpacaEval：基于LLM的自动评估</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开源模型王者vicuna,openchat, Wizardlm</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Huggingface 开放式 LLM 排行榜</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMLU只评估模型，Falcon开源项目，在Eleuther AI4个评估集上评估的LLM模型评估，vicuna建设</font></font></td>
</tr>
<tr>
<td><a href="https://opencompass.org.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opencompass.org.cn/</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上海人工智能实验室推出的开源排行榜</font></font></td>
</tr>
<tr>
<td><a href="https://lmsys.org/blog/2023-05-03-arena/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">伯克利出品大模型排位赛榜有准中文排行榜</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Elo评分机制，GPT4自然是稳居第一，GPT4&gt;克劳德&gt;GPT3.5&gt;骆驼毛&gt;其他</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/zeno-ml/zeno-build"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CMU开源聊天体育应用</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGPT&gt;Vicuna&gt;其他；在对话场景中训练可能很重要</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/zhenbench/z-bench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Z-Bench中文真格基金体育</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国产中文模型的编程可用性还相对较低，大家水平相差不多，两个版本ChatGLM提升明显</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FranxYao/chain-of-thought-hub"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思路链评估</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GSM8k、MATH等复杂题排行榜</font></font></td>
</tr>
<tr>
<td><a href="https://mp.weixin.qq.com/s?__biz=MjM5MDE0Mjc4MA==&amp;mid=2651170429&amp;idx=1&amp;sn=b98af3bd14c9f97f1aa07f0f839bb3ec&amp;scene=21#wechat_redirect" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InfoQ 大模型综合能力评估</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面向中文，ChatGPT&gt;文心一言&gt;克劳德&gt;星火</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/OpenBMB/ToolBench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ToolBench：工具调用评估清单</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具模型和ChatGPT进行对比，提供剧情脚本</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/THUDM/AgentBench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentBench：推理决策评估列表</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">清华联合多高校生产不同的任务环境，例如购物、家居、休闲等场景下模型推理决策能力</font></font></td>
</tr>
<tr>
<td><a href="https://flageval.baai.ac.cn/#/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标志评估</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源出品优势+侦查LLM评分排行榜</font></font></td>
</tr>
<tr>
<td><a href="https://bird-bench.github.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">鸟凳</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更贴合真实世界应用的大型数据库，需要领域知识的NL2SQL列表，模型追赶人类尚有时日</font></font></td>
</tr>
<tr>
<td><a href="http://103.238.162.37:31622/LeaderBoard" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科拉</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以世界知识为核心的评价基准，包括已知的百科知识和未知的近90天网络发布内容，评价知识的记忆、理解、应用和创造能力</font></font></td>
</tr>
<tr>
<td><a href="https://cevalbenchmark.com/index.html#home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">西瓦尔</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文知识评估，覆盖52个学科，机器评价主要为示范选择</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/haonan-li/CMMLU"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CMLU</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">67个主题中文知识和推理能力评估，替代选择机器评估</font></font></td>
</tr>
<tr>
<td><a href="http://llmeval.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLMEval3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复旦推出的知识问答列表，涵盖大学作业和考题，题库问题来自非互联网避免模型作弊</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Duxiaoman-DI/XuanYuan/tree/main/FinanceIQ"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">财务Q</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">度小开源的金融项目选择评估数据集</font></font></td>
</tr>
<tr>
<td><a href="https://www.swebench.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SWE-长凳</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于github真实问题和PR的模型编程能力评估</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国外开源模型</font></font></h3><a id="user-content-国外开源模型" class="anchor" aria-label="永久链接：国外开源模型" href="#国外开源模型"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型链接</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://huggingface.co/microsoft/Phi-3-mini-128k-instruct" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Φ3-MINI-128K</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还是质量&gt;数量的训练逻辑，微软的3B小模型</font></font></td>
</tr>
<tr>
<td><a href="https://llama.meta.com/llama3/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Open Meta带着可开源的羊驼3模型来了，重回王座~</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/tbressers/WizardLM-2-8x22B"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向导LM-2-8x22B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微软带来的WizardLM-2也来了包括70B，7B和8*22B</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/hpcaitech/Open-Sora"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放索拉</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没等来OpenAI却等来了OpenSora这个梗不错哦</font></font></td>
</tr>
<tr>
<td><a href="https://x.ai/blog/grok-os" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">格罗克</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">马斯克开源Grok-1：3140亿收益最大，权重架构全开放</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/google/gemma_pytorch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">芽</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">谷歌商场开源模型2B，7B免费开发</font></font></td>
</tr>
<tr>
<td><a href="https://twitter.com/MistralAI/status/1733150512395038967" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">混合8*7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法国“openai”基于MegaBlocks开源训练的MOE模型8*7B 32K</font></font></td>
</tr>
<tr>
<td><a href="https://mistral.ai/news/announcing-mistral-7b/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">米斯特拉尔7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法国“openai”Mistral，超过开源llama2当前最好7B模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/huggingface/blog/blob/main/idefics2.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Idefics2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hugging Face 推出 Idefics2 8B 多模态模型</font></font></td>
</tr>
<tr>
<td><a href="https://opencompass.org.cn/model-detail/Dolphin-2.2.1-Mistral-7B" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dolphin-2.2.1-Mistral-7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于Mistral7B使用dolphin数据集</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/tiiuae/falcon-40b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">鹘</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Falcon由外汇技术研究所在超高质量1万亿代币上训练得到1B，7B，40B开源，免费！土豪们表示钱什么的格局小了</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/lm-sys/FastChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼毛</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alpaca前成员等以LLama13B开源为基础使用ShareGPT指令变量的模型，提出了用GPT4来气压模型效果</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/imoneoi/openchat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放聊天</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">80k ShareGPT对话LLama-2 13B开源模型中的深度</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/KBlueLeaf/guanaco-7B-leh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama 7B底座，在alpaca52K数据上加入534K多语言指令数据调节</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/mosaicml/mpt-7b-chat" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MosaicML的预训练+配置开源的新模型，可运行，支持84k tokens超长输入</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/togethercomputer/RedPajama-INCITE-Instruct-3B-v1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红色睡衣</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RedPajama项目既预数据训练后开源培训3B，7B的预+指令调试模型</font></font></td>
</tr>
<tr>
<td><a href="https://bair.berkeley.edu/blog/2023/04/03/koala/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">考拉</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用alpaca，HC3等指令开源集+ ShareGPT等ChatGPT数据增量llama，在排行榜上排名靠前</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/nebuly-ai/nebullvm/tree/main/apps/accelerate/chatllama"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查拉玛</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于RLHF变量了LLaMA</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/tatsu-lab/stanford_alpaca"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">羊驼毛</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯坦福开源的使用52k数据在7B的LLaMA上API获取，</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/tloen/alpaca-lora"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">羊驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">洛拉扮演的骆驼</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/IBM/Dromedary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单峰骆驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有 LLaMA 基础的 IBM 自对齐模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/hpcaitech/ColossalAI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">巨聊</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HPC-AI Tech的Llama+RLHF开源配置</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Vision-CAIR/MiniGPT-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迷你GPT4</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vicuna+BLIP2 文本波动融合</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/trl-lib/llama-7b-se-rl-peft" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">堆栈LLama</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama使用Stackexchange数据+SFT+RL</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/cerebras/Cerebras-GPT-13B" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大脑</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cerebras开源了1亿到130亿训练的7个模型，从预数据到参数全开源</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/databricks/dolly-v2-7b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多莉-v2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可使用7b指令参数模型在GPT-J-6B开源上</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/togethercomputer/OpenChatKit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放聊天工具包</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">openai研究员打造GPT-NoX-20B+6B审计模型过滤</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/microsoft/unilm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金属LM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微软开源的大规模自监督预训练模型</font></font></td>
</tr>
<tr>
<td><a href="https://aws.amazon.com/cn/bedrock/titan/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊泰坦</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊在aws上增加自家大模型</font></font></td>
</tr>
<tr>
<td><a href="https://link.zhihu.com/?target=https%3A//github.com/facebookresearch/metaseq/tree/main/projects/OPT" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模内贴标</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Meta复刻GPT3，高达175B，但效果并不及GPT3</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/bigscience/bloom" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">盛开</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BigScience出品，规模最大176B</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/bigscience/bloomz" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">布卢姆Z</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BigScience出品，基于Bloom API</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/paperswithcode/galai"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加拉西亚</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和Bloom相似，更针对科研领域训练的模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/bigscience-workshop/t-zero"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T0</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BigScience出品，3B~11B的在T5进行指令扭矩的模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/turboderp/exllama"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EXLL妈妈</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Llama 的 Python/C++/CUDA 实现，用于 4 位 GPTQ 权重</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/lmsys/longchat-13b-16k" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">长聊</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">llama-13b使用压缩旋转嵌入技术可怕的长文本模型</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/mosaicml/mpt-30b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MPT-30B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MosaicML开源的在8Ktoken上训练的大模型</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国内开源模型</font></font></h3><a id="user-content-国内开源模型" class="anchor" aria-label="永久链接：国内开源模型" href="#国内开源模型"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型链接</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://modelscope.cn/models/deepseek-ai/DeepSeek-V2-Chat/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepSeek-v2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度求索最新发布的21B MOE超基础模型降低KV-cache高效推理更</font></font></td>
</tr>
<tr>
<td><a href="https://modelscope.cn/models/qwen/Qwen1.5-MoE-A2.7B/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qwen1.5-MoE-A2.7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qwen推出MOE版本，推理更快</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/QwenLM/Qwen1.5"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qwen1.5</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通义千问升级1.5，支持32K理解</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/baichuan-inc/Baichuan2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百川2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百川第二代也出第二个版本了，提供了7B/13B Base和聊天的版本</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/IDEA-CCNL/Ziya2-13B-Base" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">子牙2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于Llama2训练的ziya2它终于训练完了</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/InternLM/InternLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实习生LM2 7B+20B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">商汤的书生模型2支持200K</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/OpenGVLab/InternVL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实习生-VL</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最新多模态景观大模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/OrionStarAI/Orion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Orion-14B-LongChat</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">猎户星空多语言模型支持320K</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/THUDM/ChatGLM3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天GLM3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGLM3发布，支持工具调用等更多功能，不过泛化性有待评估</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/IEIT-Yuan/Yuan-2.0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元-2.0</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">浪潮发布Yuan2.0 2B，51B，102B</font></font></td>
</tr>
<tr>
<td><a href="https://www.modelscope.cn/models/01ai/Yi-6B-200k/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YI-200K</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元一智能开源超长200K的6B，34B模型</font></font></td>
</tr>
<tr>
<td><a href="https://modelscope.cn/models/xverse/XVERSE-13B-256K/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XVERSE-256K</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元象发布13B免费大模型，虽然很长但是</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FlagAlpha/Llama2-Chinese"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama2-中文</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没等太久中文预训练后的llama2来了~</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/RUC-GSAI/YuLan-Chat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">玉兰聊天2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高瓴人工智能基于Llama-2中英语继续预训练+指令双/对话强度</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/vivo-ai-lab/BlueLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">蓝色LM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vivo人工智能实验室开源大模型</font></font></td>
</tr>
<tr>
<td><a href="https://ollama.ai/library/zephyr" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">zephyr-7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HuggingFace 团队基于 UltraChat 和 UltraFeedback 训练了 Zephyr-7B 模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Xwin-LM/Xwin-LM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XWin-LM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美洲驼2 + SFT + RLHF</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/SkyworkAI/Skywork"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思凯沃</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">昆仑万维集团·天工团队开源13B大模型可合作</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/ymcui/Chinese-LLaMA-Alpaca"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中国-美洲驼-羊驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哈工大中文指令调用的LLaMA</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/OpenLMLab/MOSS"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苔藓</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为复旦正名！了预开源训练，配置配置的所有数据和模型。可运行</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/InternLM/InternLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实习生LM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">书生浦语在超过万亿代币数据上训练的多语千亿参数基础模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FlagAI-Open/Aquila2/blob/main/README_CN.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">天鹰座2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源更新Aquila2模型系列包括全新34B</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FlagAI-Open/FlagAI/tree/master/examples/Aquila"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">天鹰座</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源开源7B大模型可免费开发</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/thunlp/UltraChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超LM系列</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面壁智能开源UltraLM13B，奖励模型UltraRM，和批评模型UltraCM</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/dandelionsllm/pandallm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">熊猫LLM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLAMA2上中文wiki继续预训练+COIG配置调试</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/xverse-ai/XVERSE-13B"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XVERSE</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">据说中文超越llama2的元象开源模型13B模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Neutralzz/BiLLa"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">比拉</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama词表·增强预训练+预训练和任务1比1混合SFT+指令样本SFT三级</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FreedomIntelligence/LLMZoo"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">凤凰</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">港中文开源凤凰和奇美拉LLM，Bloom底座，40+语言支持</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/GanjinZero/wombat-7b-delta" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">袋熊-7B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">达摩院开源强化学习使用RRHF愿景的语言模型，羊驼底座</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/TigerResearch/TigerBot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">老虎机器人</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">虎博开源了7B 180B的模型以及预训练和霸语料</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/LC1332/Luotuo-Chinese-LLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文指令执行的LLaMA，和ChatGLM</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/OpenBuddy/OpenBuddy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放伙伴</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Llama 多语言对话模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Facico/Chinese-Vicuna"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中国长春花</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama 7B基座，使用Belle+Guanaco数据训练</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/CVI-SZU/Linly"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">林利</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Llama 7B底座，使用belle+guanaco+pclue+firefly+CSL+newscommentary等7个指令配置数据集训练</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/yangjianxin1/Firefly"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">萤火虫</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文2.6B模型，提升模型中文编写，古文能力，待开源全部代码，当前只有模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/project-baize/baize-chatbot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">白泽</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用100k自助聊天对话数据的LLama</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/LianjiaTech/BELLE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美女</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用ChatGPT生成数据对开源模型进行中文优化</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/search?q=chatyuan&amp;type=repositories"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">茶园</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">chatgpt后期出来国内的开源开源模型，T5架构是下面PromptCLUE的衍生模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/clue-ai/PromptCLUE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示线索</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多任务提示语言模型</font></font></td>
</tr>
<tr>
<td><a href="https://www.alice-mind.com/portal#/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">插头</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿里达摩院发布的大模型，提交申请将会下载链接</font></font></td>
</tr>
<tr>
<td><a href="https://baai.ac.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每千次展示费用2.0</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源发布CPM2.0</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/THUDM/GLM-130B"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">广义线性模型</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">清华发布的中英双语130B预训练模型</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/ictnlp/BayLing"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贝灵</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于LLama7B/13B，增强的英语/中文大语言模型的语言显示</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士免费应用</font></font></h3><a id="user-content-llm免费应用" class="anchor" aria-label="永久链接：法学硕士免费应用" href="#llm免费应用"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型链接</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://blog.perplexity.ai/blog/introducing-pplx-online-llms?utm_source=labs&amp;utm_medium=labs&amp;utm_campaign=online-llms" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PPLX-7B/70B</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Perplexity.ai的Playground支持他们自家的PPLX模型和动物SOTA模型，Gemma也支持了</font></font></td>
</tr>
<tr>
<td><a href="https://www.moonshot.cn/?ref=aihub.cn" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基米聊天</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Moonshot超长文本LLM可输入20W以上，文档总结无敌</font></font></td>
</tr>
<tr>
<td><a href="https://stepchat.cn/chats/new" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">跃问</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阶跃星立即推出了同样熟练的长文本大模型</font></font></td>
</tr>
<tr>
<td><a href="https://xinghuo.xfyun.cn/desk" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">讯飞星火</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科大讯飞</font></font></td>
</tr>
<tr>
<td><a href="https://yiyan.baidu.com/welcome" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文心一言</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百度</font></font></td>
</tr>
<tr>
<td><a href="https://tongyi.aliyun.com/qianwen/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通义千问</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿里</font></font></td>
</tr>
<tr>
<td><a href="https://www.baichuan-ai.com/chat" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百川</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百川</font></font></td>
</tr>
<tr>
<td><a href="https://chatglm.cn/main/detail" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天GLM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智谱轻言</font></font></td>
</tr>
<tr>
<td><a href="https://chat.deepseek.com/sign_in" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度搜索</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度求索</font></font></td>
</tr>
<tr>
<td><a href="https://chat.360.com/?src=ai_360_com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">360智脑</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">360</font></font></td>
</tr>
<tr>
<td><a href="https://wukong.com/tool" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">悟空</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">字节跳动</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">垂直领域模型及进展</font></font></h3><a id="user-content-垂直领域模型进展" class="anchor" aria-label="永久链接：垂直领域模型及进展" href="#垂直领域模型进展"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型链接</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://medgpt.co/home/zh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麦德GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医联发布的</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">掌中药</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google在Faln-PaLM的基础上通过多种类型的医疗QA数据进行提示调整指令获得，同时构建了MultiMedQA</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/Kent0n-Li/ChatDoctor"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天医生</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">110K真实医患对话样本+5KChatGPT生成数据进行指令调试</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/SCIR-HI/Huatuo-Llama-Med-Chinese"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">华佗</font></font></a> <a href="https://github.com/SCIR-HI/Med-ChatGLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Med-ChatGLM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学知识图谱和chatgpt构建中文医学指令数据集+医学文献和chatgpt构建多轮问答数据</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/Facico/Chinese-Vicuna/blob/master/docs/performance-medical.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中羊驼药</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chinese-vicuna在cMedQA2数据上</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/BioFM/OpenBioMed"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放生物医学</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">清华AIR开源轻量版BioMedGPT,知识图谱&amp;20+生物研究领域多模态预模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/xionghonglin/DoctorGLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">博士GLM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatDoctor+MedDialog+CMD 多轮对话+单轮指令样本 GLM</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/MediaBrain-SJTU/MedicalGPT-zh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学GPT-zh</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自建的医学数据库ChatGPT生成QA+16个情境下SELF构建场景对话</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/chaoyi-wu/PMC-LLaMA"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PMC-美洲驼</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗论文演讲 Llama</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/openmedlab/PULSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">脉冲</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bloom 表演+继续预训练</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/CogStack/OpenGPT/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NHS法学硕士</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chatgpt生成的医疗问答，对话，角色模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/michael-wzhu/ShenNong-TCM-LLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">神农医疗大模型</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以中医知识图谱的实体为中心生成的中医知识指令数据集11w+，驱动LLama-7B</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">岐黄问道大模型</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3个子模型构成，已精准疾病的临床治疗模型+基于症状的临床诊疗模型+中医养生条理模型，看起来是要ToB落地</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/SupritYoung/Zhongjing"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">仲景</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于Ziya-LLama+医疗预训练+SFT+RLHF的中文医学大模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/qiuhuachuan/smile"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微信</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心理咨询领域，通过chatgpt改写多轮对话56k</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/scutcyr/SoulChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">灵魂聊天</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心理咨询领域中文长文本指令与多轮共情对话数据联合指令参数 ChatGLM-6B</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/X-D-Lab/MindChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心灵聊天</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MindChat-Baichuan-13B、Qwen-7B、MindChat-InternLM-7B使用不同的建筑在模型安全、共情、人类价值观上进行了强化</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗</font></font></td>
<td><a href="https://github.com/FudanDISC/DISC-MedLLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DISC医学法学硕士</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">疾病知识图谱构建QA对+QA对转化成单论对话+真实世界数据重构+人类偏好数据筛选，SFT偏差百川</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://github.com/LiuHC0428/LAW-GPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LawGPT-zh</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">利用ChatGPT清洗CrimeKgAssitant数据集获得52k单轮问答+我们根据中华人民共和国法律手册上最核心的9k法律条文，利用ChatGPT联想生成具体的场景问答+知识问答使用ChatGPT基于文本构建QA对</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://github.com/pengxiao-song/LaWGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于llama+增加词表二次预训练+基于法律条款构建QA指令配置</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://github.com/AndrewZhe/lawyer-llama"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆马律师</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律指令驱动数据集：咨询+法律考试+对话进行指令驱动</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://github.com/CSHaitao/LexiLaw"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律法律</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律指令扭矩数据集：问答+书籍概念解释，法条内容进行指令扭矩数据集</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://chatlaw.cloud/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天法</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">北大推出的法律大模型，应用形式很新颖，类似于频道内流一切功能都融合在对话形式内</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律</font></font></td>
<td><a href="https://github.com/zhihaiLLM/wisdomInterrogatory"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">录问模型</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在baichuan基础上40G二次预训练+100K指令扭矩，在知识库构建上采用了Emb+意图+关键词联想结合的方案</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/CogStack/OpenGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放式GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域LLM指令样本生成+配置框架</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/ssymmetry/BBT-FinCUGE-Applications/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">乾元BigBang金融2亿模型</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融领域预训练+任务参数</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://huggingface.co/xyz-nlp/XuanYuan2.0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">度小满千亿金融大模型</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在Bloom-176B的基础上进行金融+中文预训练和权限</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/jerry1993-tech/Cornucopia-LLaMA-Fin-Chinese"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聚宝盆</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于LLaMA系基础模型经过中文金融知识指令精调/指令扭矩(Instruct-tuning)的扭矩模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/chancefocus/PIXIU"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">貔貅</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">整理了多个金融任务数据集加入了时间序列数据进行指令参数</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/AI4Finance-Foundation/FinGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">芬GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融传统任务调用或chatgpt生成金融工具调用</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/TongjiFinLab/CFGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CFGPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融预训练+指令指令+RAG等检索任务增强</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/FudanDISC/DISC-FinLLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DISC金融法学硕士</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复旦发布多模型组合金融系统，包括金融知识问答、金融NLP任务、金融计算、金融搜索问答</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://github.com/AbaciNLP/InvestLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">投资管理公司</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CFA考试，SEC，StackExchange投资问题等构建的金融配置驱动LLaMA-65+</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><a href="https://sota.jiqizhixin.com/project/deepmoney" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深钱</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于yi-34b-200k使用金融研报进行闹钟</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="https://github.com/bigcode-project/starcoder"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">星编码器</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">80种Smashing语言+Issue+Commit训练得到的Smashing大模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="https://github.com/cubenlp/ChatSQL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天SQL</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于ChatGLM实现NL2sql</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="http://keg.cs.tsinghua.edu.cn/codegeex/index_zh.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码吉克斯</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">13B预训练+多语言变大模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="https://github.com/THUDM/CodeGeeX2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码吉克斯2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chatglm2的基础上CodeGeeX2-6B进一步经过了600B代码数据预</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="https://stability.ai/blog/stablecode-llm-generative-ai-coding" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定代码</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">560B token多语言预训练+ 120,000个Alpaca指令箭头</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编程</font></font></td>
<td><a href="https://github.com/defog-ai/sqlcoder"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL编码器</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在StarCoder的基础上扭矩15B超越gpt3.5</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><a href="https://www.mathgpt.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是好的未来自主研发的，面向全球数学爱好者和科研机构，以解题和讲题算法为核心的大模型。</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><a href="https://tiger-ai-lab.github.io/MAmmoTH/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">长毛象</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过COT+POT构建了MathInstruct数据集骆驼在OOD数据集上超越了WizardLM</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><a href="https://github.com/meta-math/MetaMath"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元数学</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型逆向思维解决数学问题，构建了新的MetaMathQA 控制器 llama2</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交通</font></font></td>
<td><a href="https://github.com/DUOMO/TransGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">转GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama-7B+34.6万领域预训练+5.8万条领域指令对话（来自文档问答）</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交通</font></font></td>
<td><a href="https://github.com/lijlansg/TrafficGPT/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交通GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGPT+Prompt实现规划，调用交通流量领域专业TFM模型，TFM负责数据分析，任务执行，可视化等操作</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科技</font></font></td>
<td><a href="https://github.com/gmftbyGMFTBY/science-llm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">墨子</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红睡衣预训练+论文QA数据集+ChatGPT补充科研对话数据</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">天文</font></font></td>
<td><a href="https://github.com/Yu-Yang-Li/StarGLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">星型GLM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">天文知识指令配置，项目进行中高级考虑天文二次预训练+KG</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科</font></font></td>
<td><a href="https://www.zhihu.com/question/613058630" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读文-网文大模型介绍</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">签约作者内测中，主打的内容为打斗场景、剧情切换、环境描绘、人物设定、世界观等辅助片段的生成</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科</font></font></td>
<td><a href="https://github.com/search?q=MediaGPT&amp;type=repositories"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLama-7B增强词表+指令强度，指令来自国内媒体专家给出的在新闻创作上的80个子任务</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">电商</font></font></td>
<td><a href="https://github.com/Alibaba-NLP/EcomGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生态GPT</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">电商领域任务指令负载大模型，指令样本250万，基础模型是Bloomz</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">植物科学</font></font></td>
<td><a href="https://www.aminer.cn/pub/6596236b939a5f408292e52a/pllama-an-open-source-large-language-model-for-plant-science" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聚乳酸</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于Llama使用植物科学领域学术论文继续预训练+sft扩展的领域模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估</font></font></td>
<td><a href="https://modelscope.cn/models/lockonlvange/autoj-13b-fp16/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动J</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上交开源进行了价值评估13B模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估</font></font></td>
<td><a href="https://github.com/baaivision/JudgeLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法官LM</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源开源了 JudgeLM 的裁判模型，可以准确地评价高效判罚主流模型</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估</font></font></td>
<td><a href="https://github.com/thu-coai/CritiqueLLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士批判</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智谱AI发布评分模型CritiqueLLM，支持含参考文本/无参考文本的评估打分</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具和库</font></font></h2><a id="user-content-tool-and-library" class="anchor" aria-label="永久链接：工具和库" href="#tool-and-library"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推理框架</font></font></h3><a id="user-content-推理框架" class="anchor" aria-label="永久链接：推理框架" href="#推理框架"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FlexFlow：模型部署推理框架</font></font></td>
<td><a href="https://github.com/flexflow/FlexFlow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/flexflow/FlexFlow</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Medusa：针对采样解码的推理加速框架，可以和策略其他结合</font></font></td>
<td><a href="https://github.com/FasterDecoding/Medusa"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FasterDecoding/Medusa</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FlexGen：LLM 推理 CPU 卸载计算架构</font></font></td>
<td><a href="https://github.com/FMInference/FlexGen"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FMInference/FlexGen</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">VLLM：超高速推理框架Vicuna，竞技场背后的无名英雄，比HF快24倍，支持很多基座模型</font></font></td>
<td><a href="https://github.com/vllm-project/vllm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/vllm-project/vllm</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Streamingllm：新的注意力池注意力方案，耗费更多拓展模型推理长度，同时为推理提速</font></font></td>
<td><a href="https://github.com/mit-han-lab/streaming-llm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/mit-han-lab/streaming-llm</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">llama2.c: llama2纯C语言的推理框架</font></font></td>
<td><a href="https://github.com/karpathy/llama2.c"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/karpathy/llama2.c</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数，预训练，rlhf框架</font></font></h3><a id="user-content-指令微调预训练rlhf框架" class="anchor" aria-label="永久链接：指令配置，预训练，rlhf框架" href="#指令微调预训练rlhf框架"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LoRA：低阶指令参数方案</font></font></td>
<td><a href="https://github.com/tloen/alpaca-lora"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/tloen/alpaca-lora</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">peft：参数高效的提示调整工具集</font></font></td>
<td><a href="https://github.com/huggingface/peft"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/huggingface/peft</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RL4LM：AllenAI 的 RL 工具</font></font></td>
<td><a href="https://github.com/allenai/RL4LMs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/allenai/RL4LMs</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLLTE：港大，大疆等联合开源RLLTE开源学习框架</font></font></td>
<td><a href="https://github.com/RLE-Foundation/rllte"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/RLE-Foundation/rllte</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">trl：基于Transformer的强化训练框架</font></font></td>
<td><a href="https://github.com/lvwerra/trl"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/lvwerra/trl</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">trlx：环球训练trl</font></font></td>
<td><a href="https://github.com/CarperAI/trlx"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/CarperAI/trlx</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">北大开源河狸项目可复现RLHF，支持大多数LLM，提供RLHF数据</font></font></td>
<td><a href="https://github.com/PKU-Alignment/safe-rlhf"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/PKU-Alignment/safe-rlhf</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RL4LM：AllenAI 的 RL 工具</font></font></td>
<td><a href="https://github.com/allenai/RL4LMs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/allenai/RL4LMs</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LMFlow：港科大实验室的开源模型配置框架，支持多数开源模型的配置和RLHF</font></font></td>
<td><a href="https://github.com/OptimalScale/LMFlow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OptimalScale/LMFlow</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HuggingNLP：基于Huggingface开发继承Prompt技术，预训练和是指输入等多种方案</font></font></td>
<td><a href="https://github.com/wjn1996/HugNLP"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/wjn1996/HugNLP</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Deepspeed：针对RL训练和推理的整合优化</font></font></td>
<td><a href="https://github.com/microsoft/DeepSpeed"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/DeepSpeed</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Uerpy：预训练框架支持lm、mlm、unilm等</font></font></td>
<td><a href="https://github.com/dbiir/UER-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/dbiir/UER-py</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TecentPretrain：Uerpy的重构版本支持llama预训练</font></font></td>
<td><a href="https://github.com/Tencent/TencentPretrain/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Tencent/TencentPretrain/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">lamini：整合指令数据生成，SFT，RLHF的工具库</font></font></td>
<td><a href="https://github.com/lamini-ai/lamini/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/lamini-ai/lamini/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chain-of-thought-hub：模型推理能力评估平台</font></font></td>
<td><a href="https://github.com/FranxYao/chain-of-thought-hub"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FranxYao/chain-of-thought-hub</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EasyEdit：浙大开源支持多种模型，多种方案的模型知识精准编辑器</font></font></td>
<td><a href="https://github.com/zjunlp/EasyEdit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/zjunlp/EasyEdit</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenDelta：集成了各种增量需求方案的开源实现</font></font></td>
<td><a href="https://github.com/thunlp/OpenDelta"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thunlp/OpenDelta</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Megablocks：教育部训练框架</font></font></td>
<td><a href="https://github.com/stanford-futuredata/megablocks"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/stanford-futuredata/megablocks</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">授课对象：教育部培训框架</font></font></td>
<td><a href="https://github.com/microsoft/tutel"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/tutel</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LongLora：长文本框架框架</font></font></td>
<td><a href="https://github.com/dvlab-research/LongLoRA"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/dvlab-research/LongLoRA</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LlamaGym：在线RL框架框架</font></font></td>
<td><a href="https://github.com/KhoomeiK/LlamaGym"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/KhoomeiK/LlamaGym</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Megatron-LM：主流LLM预训练框架</font></font></td>
<td><a href="https://github.com/NVIDIA/Megatron-LM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/NVIDIA/Megatron-LM</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TradingGym：参考openaigym的股票交易强化学习模拟器</font></font></td>
<td><a href="https://github.com/astrologos/tradinggym"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/astrologos/tradinggym</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TradeMaster：量化交易RL训练框架</font></font></td>
<td><a href="https://github.com/TradeMaster-NTU/TradeMaster"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/TradeMaster-NTU/TradeMaster</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动/多代理</font></font></h3><a id="user-content-automulti-agent" class="anchor" aria-label="永久链接：自动/多代理" href="#automulti-agent"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoGen：开源多Agent框架框架</font></font></td>
<td><a href="https://github.com/microsoft/autogen"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/autogen</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CrewAI：比chatDev流程定义了更灵活的多智能体框架</font></font></td>
<td><a href="https://github.com/joaomdmoura/CrewAI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/joaomdmoura/CrewAI</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatDev：面壁智能多开源智能体协作的虚拟软件公司</font></font></td>
<td><a href="https://github.com/OpenBMB/ChatDev"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/ChatDev</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Generative Agents：斯坦福AI小镇的开源代码</font></font></td>
<td><a href="https://github.com/joonspk-research/generative_agents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/joonspk-research/generative_agents</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BabyAGI：自执行LLM代理</font></font></td>
<td><a href="https://github.com/yoheinakajima/babyagi"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/yoheinakajima/babyagi</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoGPT：自执行LLM代理</font></font></td>
<td><a href="https://github.com/Torantulino/Auto-GPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Torantulino/Auto-GPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoGPT-Plugins：提供大众Auo-GPT官方和第三方的插件</font></font></td>
<td><a href="https://github.com/Significant-Gravitas/Auto-GPT-Plugins"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Significant-Gravitas/Auto-GPT-Plugins</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XAgent：面壁智能双开源循环AutoGPT</font></font></td>
<td><a href="https://github.com/OpenBMB/XAgent"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/XAgent</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MetaGPT：覆盖软件公司全生命流程，例如产品经理等各个职业的AutoGPT</font></font></td>
<td><a href="https://github.com/geekan/MetaGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/geekan/MetaGPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ResearchGPT：AutoGPT领域的论文写作，融合论文拆解+网络爬虫</font></font></td>
<td><a href="https://github.com/assafelovic/gpt-researcher"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/assafelovic/gpt-researcher</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MiniAGI：自执行LLM代理</font></font></td>
<td><a href="https://github.com/muellerberndt/mini-agi"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/muellerberndt/mini-agi</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AL Legion： 自执行LLM代理</font></font></td>
<td><a href="https://github.com/eumemic/ai-legion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/eumemic/ai-legion</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentVerse：多模型交互环境</font></font></td>
<td><a href="https://github.com/OpenBMB/AgentVerse"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/AgentVerse</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentSims：给定一个社会环境，评估LLM作为智能体的预定任务目标完成能力的沙盒环境</font></font></td>
<td><a href="https://github.com/py499372727/AgentSims/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/py499372727/AgentSims/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTRPG：RPG环境AI代理游戏化</font></font></td>
<td><a href="https://github.com/dzoba/gptrpg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/dzoba/gptrpg</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTeam：多智能体交互</font></font></td>
<td><a href="https://github.com/101dotxyz/GPTeam"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/101dotxyz/GPTeam</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTEngineer：自动工具构建和代码生成</font></font></td>
<td><a href="https://github.com/AntonOsika/gpt-engineer"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/AntonOsika/gpt-engineer</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WorkGPT：类似AutoGPT</font></font></td>
<td><a href="https://github.com/team-openpm/workgpt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/team-openpm/workgpt</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI-Town：虚拟世界模拟器</font></font></td>
<td><a href="https://github.com/a16z-infra/ai-town"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/a16z-infra/ai-town</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">webarena：网络拟真环境，可用于自主智能体的测试，支持在线购物，论坛，代码仓库等</font></font></td>
<td><a href="https://github.com/web-arena-x/webarena"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/web-arena-x/webarena</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MiniWoB++：100+web交互的拟真环境</font></font></td>
<td><a href="https://github.com/Farama-Foundation/miniwob-plusplus"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Farama-Foundation/miniwob-plusplus</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">VIRL：虚拟世界模拟器</font></font></td>
<td><a href="https://github.com/VIRL-Platform/VIRL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/VIRL-Platform/VIRL</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Agent工具框架类</font></font></h3><a id="user-content-agent工具框架类" class="anchor" aria-label="永久链接：Agent工具框架类" href="#agent工具框架类"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAgents：ChatGPT开源-Plus版搭建框架</font></font></td>
<td><a href="https://github.com/xlang-ai/OpenAgents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/xlang-ai/OpenAgents</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LangGraph：白盒话，可循环基于有向无环图的Agent工作流构建框架</font></font></td>
<td><a href="https://langchain-ai.github.io/langgraph/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://langchain-ai.github.io/langgraph/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">langchain：LLM代理框架</font></font></td>
<td><a href="https://github.com/hwchase17/langchain"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/hwchase17/langchain</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">llama索引：LLM代理框架</font></font></td>
<td><a href="https://github.com/jerryjliu/llama_index"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/jerryjliu/llama_index</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Langroid：LLM代理框架</font></font></td>
<td><a href="https://github.com/langroid/langroid"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/langroid/langroid</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ragas：评估搜索增强LLM效果的框架，基于大模型及时评估事实、认知相关性、认知内容质量、回答相关性等</font></font></td>
<td><a href="https://github.com/explodinggradients/ragas#fire-quickstart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/explodinggradients/ragas#fire-quickstart</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">fastRAG：搜索框架，包括多索引搜索，KG构建等基础功能</font></font></td>
<td><a href="https://github.com/IntelLabs/fastRAG/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/IntelLabs/fastRAG/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">langflow：把langchain等agent组件外接了可拖拽式的UI</font></font></td>
<td><a href="https://github.com/logspace-ai/langflow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/logspace-ai/langflow</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PhiData：把工具调用抽象成函数调用的Agent框架</font></font></td>
<td><a href="https://github.com/phidatahq/phidata"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/phidatahq/phidata</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Haystack：LLM Agent框架，pipeline的设计模式个人感觉比langchain更灵活更简洁</font></font></td>
<td><a href="https://github.com/deepset-ai/haystack"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/deepset-ai/haystack</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EdgeChain：通过Jsonnet配置文件实现LLM代理</font></font></td>
<td><a href="https://github.com/arakoodev/EdgeChains/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/arakoodev/EdgeChains/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语义内核：集成模型和编程语言的SDK</font></font></td>
<td><a href="https://github.com/microsoft/semantic-kernel"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/semantic-kernel</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BMTTools：清华出品多工具调用开源库，提供第三方数据和评估ToolBench</font></font></td>
<td><a href="https://github.com/OpenBMB/BMTools"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/BMTools</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Jarvis：大模型调用小模型框架，给小模型一个未来！</font></font></td>
<td><a href="https://github.com/search?q=jarvis"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/search?q=jarvis</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM-ToolMaker:让LLM自己制造代理</font></font></td>
<td><a href="https://github.com/ctlllll/LLM-ToolMaker"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ctlllll/LLM-ToolMaker</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gorilla：LLM调用大量API</font></font></td>
<td><a href="https://github.com/ShishirPatil/gorilla"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ShishirPatil/gorilla</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Open-Interpreter：命令行聊天框架</font></font></td>
<td><a href="https://github.com/KillianLucas/open-interpreter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/KillianLucas/open-interpreter</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AnythingLLM：langchain推出的支持本地开源部署模型的框架</font></font></td>
<td><a href="https://github.com/Mintplex-Labs/anything-llm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Mintplex-Labs/anything-llm</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PromptFlow：微软推出的大型模型应用框架</font></font></td>
<td><a href="https://github.com/microsoft/promptflow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/promptflow</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Anakin：和 Coze 类似的代理定制应用程序，插件支持很少，但工作流程使用起来更简洁</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">r</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TaskingAI：面向API的类似langchain的大模型应用框架</font></font></td>
<td><a href="https://www.tasking.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.tasking.ai/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TypeChat：微软推出的Schema Engineering风格的应用框架</font></font></td>
<td><a href="https://github.com/microsoft/TypeChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/microsoft/TypeChat</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DSPy：将稳定性低的提示优化为参数化和模板化的提示技术</font></font></td>
<td><a href="https://github.com/stanfordnlp/dspy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/stanfordnlp/dspy</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Agent Bot [托拉拽中间层]</font></font></h3><a id="user-content-agent-bot-托拉拽中间层" class="anchor" aria-label="永久链接：Agent Bot [托拉拽中间层]" href="#agent-bot-托拉拽中间层"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迪菲</font></font></td>
<td><a href="https://dify.ai/zh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://dify.ai/zh</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科兹</font></font></td>
<td><a href="https://www.coze.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.coze.com/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿纳金</font></font></td>
<td><a href="https://app.anakin.ai/discover" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://app.anakin.ai/discover</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">福洛威斯</font></font></td>
<td><a href="https://github.com/FlowiseAI/Flowise/blob/main/README-ZH.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FlowiseAI/Flowise/blob/main/README-ZH.md</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAG安装工具</font></font></h3><a id="user-content-rag配套工具" class="anchor" aria-label="永久链接：RAG 安装工具" href="#rag配套工具"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://alex.macrocosm.so/download" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚历山大港</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来自Arix论文开始将整个互联网变成支持索引，可以免费下载</font></font></td>
</tr>
<tr>
<td><a href="https://rapidapi.com/hub" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速API</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">统一这个世界上的所有API，最大的API Hub，有调用成功率，延迟等，真是爱！</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/tesseract-ocr/tesseract"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTesseract</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OCR解析服务</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/JaidedAI/EasyOCR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">易捷OCR</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确实使用了非常友好的 OCR 服务</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/tesseract-ocr/tesseract"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">各不相同</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">旷视多模态大模型pdf直接转Markdown</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/run-llama/llama_parse?tab=readme-ov-file"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼解析</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLamaIndex提供的PDF解析服务，每天免费1000篇</font></font></td>
</tr>
<tr>
<td><a href="https://link.zhihu.com/?target=https%3A//huggingface.co/jinaai/jina-embeddings-v2-base-zh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">吉娜-科伯特</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">健AI开源中英德，8192 Token长文本嵌入</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/FlagOpen/FlagEmbedding/blob/master/FlagEmbedding/BGE_M3/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BGE-M3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源开源多语言，稀疏+稠密表征，8192 Token长文本嵌入</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/netease-youdao/BCEmbedding/blob/master/README_zh.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">公元前</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网易开源更重构RAG任务的Embedding模型</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/LinWeizheDragon/PreFLMR_ViT-G" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前FLMR-VIT-G</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">剑桥开源多模态Retriever</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Filimoa/open-parse?tab=readme-ov-file"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放解析</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本解析分块服务，先分析文档的开源布局再进行切分</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/Layout-Parser/layout-parser"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">布局解析器</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">准确度排名第一的开源OCR文档布局识别</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/AlibabaResearch/AdvancedLiterateMachinery"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先进机械</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿里OCR团队的文档解析和图片理解</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/infiniflow/ragflow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ragflow-deepdoc</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ragflow提供文档识别和解析能力</font></font></td>
</tr>
<tr>
<td><a href="https://rili.jin10.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">火爬行</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">爬取url并生成markdown的神器</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/SuperpoweredAI/spRAG"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随机RAG</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注入上下文表征，和自动连接上下文提高边界</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他垂直领域代理</font></font></h3><a id="user-content-其他垂直领域agent" class="anchor" aria-label="永久链接：其他垂直领域代理" href="#其他垂直领域agent"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT4v-ACT：基于JS DOM识别网页元素，服务于多种模式的webagent</font></font></td>
<td><a href="https://github.com/ddupont808/GPT-4V-Act?tab=readme-ov-file"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ddupont808/GPT-4V-Act?tab=readme-ov-file</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Deep-KE：基于LLM对数据进行智能解析实现知识抽取</font></font></td>
<td><a href="https://github.com/zjunlp/DeepKE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/zjunlp/DeepKE</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IncarnaMind：多文档RAG方案，动态分块的方案可以收集</font></font></td>
<td><a href="https://github.com/junruxiong/IncarnaMind"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/junruxiong/IncarnaMind</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vectra：平台化的LLM代理搭建方案，从索引构建，内容认知排序，到事实检查的LLM生成</font></font></td>
<td><a href="https://vectara.com/tour-vectara/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://vectara.com/tour-vectara/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Data-Copilot：时间序列等重构数据分析领域的Agent解决方案</font></font></td>
<td><a href="https://github.com/zwq2018/Data-Copilot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/zwq2018/Data-Copilot</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DB-GPT：以数据库为基础的GPT实验项目，使用本地化的GPT大模型与您的数据和环境进行交互</font></font></td>
<td><a href="https://db-gpt.readthedocs.io/projects/db-gpt-docs-zh-cn/zh_CN/latest/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://db-gpt.readthedocs.io/projects/db-gpt-docs-zh-cn/zh_CN/latest/index.html</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Guardrails：降低模型幻觉的python框架，提示模板+验证+修改</font></font></td>
<td><a href="https://github.com/shreyar/guardrails"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/shrear/guardrails</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指导：微软新开源框架，同样是降低模型幻觉的框架，提示+链的升级版加入渐进生成和思维流程图</font></font></td>
<td><a href="https://github.com/guidance-ai/guidance"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/guidance-ai/guidance</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SolidGPT：上传个人数据，通过命令交互创建项目PRD等</font></font></td>
<td><a href="https://github.com/AI-Citizen/SolidGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/AI-Citizen/SolidGPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HR-Agent：类似HR和员工交互，支持多工具调用</font></font></td>
<td><a href="https://github.com/stepanogil/autonomous-hr-chatbot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/stepanogil/autonomous-hr-chatbot</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BambooAI：数据分析Agent</font></font></td>
<td><a href="https://github.com/pgalko/BambooAI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/pgalko/BambooAI</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlphaCodium：通过流程工程完成代码任务</font></font></td>
<td><a href="https://github.com/Codium-ai/AlphaCodium"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Codium-ai/AlphaCodium</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REOR：AI驱动的笔记软件</font></font></td>
<td><a href="https://github.com/reorproject/reor"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/reorproject/reor</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vanna.AI：与sql数据库聊天</font></font></td>
<td><a href="https://vanna.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://vanna.ai/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">融合了图逻辑和LLM的高效爬虫</font></font></td>
<td><a href="https://scrapegraph-doc.onrender.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://scrapegraph-doc.onrender.com/</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练数据</font></font></h2><a id="user-content-training-data" class="anchor" aria-label="永久链接：训练数据" href="#training-data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据类型</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据描述</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自指令，GPT3自动生成&amp;过滤获取指令集</font></font></td>
<td><a href="https://github.com/yizhongw/self-instruct"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/yizhongw/self-instruct</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Standford Alpaca：52K text-davinci-003生成的自指令指令数据集</font></font></td>
<td><a href="https://github.com/tatsu-lab/stanford_alpaca"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/tatsu-lab/stanford_alpaca</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT4-for-LLM 中文+中文+对比指令</font></font></td>
<td><a href="https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTteacher更多样的通用指令角色，播放和代码指令</font></font></td>
<td><a href="https://github.com/teknium1/GPTeacher/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/teknium1/GPteacher/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文翻译羊驼还有一些其他指令数据集</font></font></td>
<td><a href="https://github.com/hikariming/alpaca_chinese_dataset"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/hikariming/alpaca_chinese_dataset </font></font></a> <a href="https://github.com/carbonz0/alpaca-chinese-dataset"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/carbonz0/alpaca-chinese-dataset</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">alpaca指令GPT4生成，和以上几个版本对比显着质量更高，回复更长</font></font></td>
<td><a href="https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">guanaco数据：对Alphca指令重写后以不同语言生成总共534K，有对话和非对话类型，还有补充的QA生成样本</font></font></td>
<td><a href="https://huggingface.co/datasets/JosephusCheung/GuanacoDataset" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/JosephusCheung/GuanacoDataset</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OIG中文指令包括翻译羊驼+自然+非自然，多轮对话，考试，leetcode指令</font></font></td>
<td><a href="https://github.com/BAAI-Zlab/COIG"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/BAAI-Zlab/COIG</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vicuna 训练使用的样本，用API获取了sharegpt上用户和chatgpt对话历史，部分网友整理到了HF</font></font></td>
<td><a href="https://github.com/domeccleston/sharegpt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/domeccleston/sharegpt </font></font></a> <a href="https://huggingface.co/datasets/anon8231489123/ShareGPT_Vicuna_unfiltered/tree/main" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/anon8231489123/ShareGPT_Vicuna_unfiltered/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HC3指令数据中英文，包括金融，开放QA，百科，DBQA，医学等包含人工回复</font></font></td>
<td><a href="https://huggingface.co/datasets/Hello-SimpleAI/HC3-Chinese/tree/main" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/Hello-SimpleAI/HC3-Chinese/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MOSS开源的SFT数据包含使用插件的对话数据</font></font></td>
<td><a href="https://huggingface.co/datasets/Hello-SimpleAI/HC3-Chinese/tree/main" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/Hello-SimpleAI/HC3-Chinese/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructWildData：用潜爬取的chatgpt指令作为种子self-instruct补充生成，中英文双语</font></font></td>
<td><a href="https://github.com/XueFuzhao/InstructionWild/tree/main/data"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/XueFuzhao/InstructionWild/tree/main/data</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BELLE100万指令数据，参考Alpaca用ChatGPT生成，有数学，多轮对话，校色对话等等</font></font></td>
<td><a href="https://github.com/LianjiaTech/BELLE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/LianjiaTech/百丽</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PromptCLUE多任务提示数据集：模板构建，只包含标准NLP任务</font></font></td>
<td><a href="https://github.com/CLUEbenchmark/pCLUE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/CLUEbenchmark/pCLUE</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TK-Instruct微调用的指令数据集，全人工标签1600+NLP任务</font></font></td>
<td><a href="https://instructions.apps.allenai.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://instructions.apps.allenai.org/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T0微调用的指令数据集（P3）</font></font></td>
<td><a href="https://huggingface.co/datasets/bigscience/P3" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/bigscience/P3</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">p3衍生的46种多语言数据集（xmtf）</font></font></td>
<td><a href="https://github.com/bigscience-workshop/xmtf"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/bigscience-workshop/xmtf</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非自然指令使用GPT3生成后改写得到240k</font></font></td>
<td><a href="https://github.com/orhonovich/unnatural-instructions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/orhonovich/unnatural-instructions</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">羊驼COT对多个数据源进行了清理并统一格式推送的了HF，重点是人工整理的COT数据</font></font></td>
<td><a href="https://github.com/PhoebusSi/Alpaca-CoT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/PhoebusSi/Alpaca-CoT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人工编写包含23种常见的中文NLP任务的指令数据，中文书写方向</font></font></td>
<td><a href="https://github.com/yangjianxin1/Firefly"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/yangjianxin1/Firefly</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Amazon COT 指令样本包括各类 QA，bigbench，math 等</font></font></td>
<td><a href="https://github.com/amazon-science/auto-cot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/amazon-science/auto-cot</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CSL包含396,209篇中文核心期刊论文元信息（标题、摘要、关键词、学科、门类）可做预训练可构建NLP指令任务</font></font></td>
<td><a href="https://github.com/ydli-ai/CSL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ydli-ai/CSL</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">羊驼代码 20K代码指令数据</font></font></td>
<td><a href="https://github.com/sahil280114/codealpaca#data-release"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/sahil280114/codealpaca#data-release</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT4Tools 71K GPT4 指令样本</font></font></td>
<td><a href="https://github.com/StevenGrove/GPT4Tools"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/StevenGrove/GPT4Tools</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT4指令+玩角色+代码指令</font></font></td>
<td><a href="https://github.com/teknium1/GPTeacher"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/teknium1/GPTeacher</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mol-Instructions 2043K 分子+蛋白质+生物分子文本指令，覆盖分子设计、蛋白质功能预测、蛋白质设计等任务</font></font></td>
<td><a href="https://github.com/zjunlp/Mol-Instructions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/zjunlp/Mol-Instructions</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">腾讯人工智能实验室发布网上爬取数学题APE210k</font></font></td>
<td><a href="https://github.com/Chenny0808/ape210k"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Chenny0808/ape210k</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">猿辅导AI Lab小学应用题Math23K</font></font></td>
<td><a href="https://github.com/SCNU203/Math23k/tree/main"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/SCNU203/Math23k/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">小学数学把 OpenAI 的高中数学题改造成指令样本有 2-8 步推理过程</font></font></td>
<td><a href="https://huggingface.co/datasets/qwedsacf/grade-school-math-instructions" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/qwedsacf/grade-school-math-instructions</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学问答数据集有推理过程和演示选择</font></font></td>
<td><a href="https://huggingface.co/datasets/math_qa/viewer/default/test?row=2" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/math_qa/viewer/default/test?row=2</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AMC竞赛数学题</font></font></td>
<td><a href="https://huggingface.co/datasets/competition_math" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/competition_math</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数学</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">线性代数等纯数学计算题</font></font></td>
<td><a href="https://huggingface.co/datasets/math_dataset" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/math_dataset</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来自不同的开放访问编码网站Codeforces、Kattis等收集的问题的APPS</font></font></td>
<td><a href="https://opendatalab.org.cn/APPS" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opendatalab.org.cn/APPS</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LyraCode由带有嵌入式SQL的Python代码组成，经过仔细注释的数据库操作程序，以及中文评论和中文评论。</font></font></td>
<td><a href="https://opendatalab.org.cn/Lyra" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opendatalab.org.cn/Lyra</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Conala来自StackOverflow问题，手动注释3k，中文</font></font></td>
<td><a href="https://opendatalab.org.cn/CoNaLa/download" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opendatalab.org.cn/CoNaLa/download</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">code-alpaca ChatGPT 生成20K代码指令样本</font></font></td>
<td><a href="https://github.com/sahil280114/codealpaca.git"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/sahil280114/codealpaca.git</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">32K，四种不同类型、不同分量的代码相关中文对话数据，有大模型生成，</font></font></td>
<td><a href="https://github.com/zxx000728/CodeGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/zxx000728/CodeGPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LAION 策划的开放指令通用数据集中手动选择的组件子集已开源40M 3万个,100M在路上</font></font></td>
<td><a href="https://github.com/LAION-AI/Open-Instruction-Generalist"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/LAION-AI/Open-Instruction-Generalist</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百泽基于Chat GPT构建的自聊天数据</font></font></td>
<td><a href="https://github.com/project-baize/baize-chatbot/tree/main/data"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/project-baize/baize-chatbot/tree/main/data</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FaceBook开源BlenderBot训练对话数据~6K</font></font></td>
<td><a href="https://huggingface.co/datasets/blended_skill_talk" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/blished_skill_talk</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AllenAI开源38.5万个对话高质量数据集SODA</font></font></td>
<td><a href="https://realtoxicityprompts.apps.allenai.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://realoxityprompts.apps.allenai.org/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructDial 在单一对话任务类型上进行配置配置</font></font></td>
<td><a href="https://github.com/prakharguptaz/Instructdial"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/prakarguptaz/Instructdial</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ultra Chat 两个独立的 ChatGPT Turbo API 进行对话，从而生成多轮对话数据</font></font></td>
<td><a href="https://github.com/thunlp/UltraChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thunlp/UltraChat</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">很棒的开放域对话模型提供了多个开放域对话数据</font></font></td>
<td><a href="https://github.com/cingtiye/Awesome-Open-domain-Dialogue-Models#%E4%B8%AD%E6%96%87%E5%BC%80%E6%94%BE%E5%9F%9F%E5%AF%B9%E8%AF%9D%E6%95%B0%E6%8D%AE%E9%9B%86"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/cingtiye/Awesome-Open-domain-Dialogue-Models#%E4%B8%AD%E6%96%87%E5%BC%80%E6%94%BE%E5%9F%9F %E5%AF%B9%E8%AF%9D%E6%95%B0%E6%8D%AE%E9%9B%86</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Salesforce开源超全DialogStudio</font></font></td>
<td><a href="https://github.com/salesforce/DialogStudio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/salesforce/DialogStudio</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于事实参考的多轮问答中文数据，已开源5万，之后会开源更多</font></font></td>
<td><a href="https://github.com/sufengniu/RefGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/sufengniu/RefGPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">右肺出血</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">北大河狸RLHF开源数据集10K，1M需要申请</font></font></td>
<td><a href="https://huggingface.co/datasets/PKU-Alignment/PKU-SafeRLHF-10K" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/PKU-Alignment/PKU-SafeRLHF-10K</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人类 hh-rlhf 数据集</font></font></td>
<td><a href="https://huggingface.co/datasets/Anthropic/hh-rlhf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/Anthropic/hh-rlhf</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stack-exchange上问题回答，每个答案都有打分多个</font></font></td>
<td><a href="https://huggingface.co/datasets/HuggingFaceH4/stack-exchange-preferences/tree/main" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/HuggingFaceH4/stack-exchange-preferences/tree/main</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Facebook Bot 对抗性对话数据集 5K</font></font></td>
<td><a href="https://github.com/facebookresearch/ParlAI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/facebookresearch/ParlAI</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AllenAI真实毒性提示</font></font></td>
<td><a href="https://github.com/facebookresearch/ParlAI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/facebookresearch/ParlAI</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAssistant对话160K消息，13500人工生成，中文主控</font></font></td>
<td><a href="https://huggingface.co/datasets/OpenAssistant/oasst1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/OpenAssistant/oasst1</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知乎选择偏好数据集</font></font></td>
<td><a href="https://huggingface.co/datasets/liyucheng/zhihu_rlhf_3k" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/liyu Cheng/zhihu_rlhf_3k</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hh-rlhf中文翻译偏好数据</font></font></td>
<td><a href="https://huggingface.co/datasets/liswei/rm-static-zhTW" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/liswei/rm-static-zhTW</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面壁智能开源大规模偏好数据，基于64K提示使用不同模型生成4个回答使用GPT-4评估</font></font></td>
<td><a href="https://github.com/OpenBMB/UltraFeedback"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/UltraFeedback</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BigBench(超越模仿游戏基准)</font></font></td>
<td><a href="https://github.com/google/BIG-bench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/google/BIG-bench</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复杂的 QA：用于 ChatGPT 的体育指令集</font></font></td>
<td><a href="https://github.com/tan92hl/Complex-Question-Answering-Evaluation-of-ChatGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/tan92hl/Complex-Question-Answering-Evaluation-of-ChatGPT</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Langchain开源评估数据集</font></font></td>
<td><a href="https://huggingface.co/LangChainDatasets" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/LangChainDatasets</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2010-2022年全国高考卷的题目</font></font></td>
<td><a href="https://github.com/OpenLMLab/GAOKAO-Bench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenLMLab/GAOKAO-Bench</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文通用大模型综合性体育基准SuperCLUE</font></font></td>
<td><a href="https://github.com/CLUEbenchmark/SuperCLUE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/CLUEbenchmark/SuperCLUE</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RedPajama开源的复刻llama的预训练数据集，1.21万亿Token</font></font></td>
<td><a href="https://github.com/togethercomputer/RedPajama-Data"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/togethercomputer/RedPajama-Data</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cerebras 基于RedPajama进行清洗去重后得到的高质量数据集，6270亿Token</font></font></td>
<td><a href="https://huggingface.co/datasets/cerebras/SlimPajama-627B/tree/main/train" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/cerebras/SlimPajama-627B/tree/main/train</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">堆22个高质量数据集混合的预训练数据集800G,全量开放下载</font></font></td>
<td><a href="https://pile.eleuther.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://pile.eleuther.ai/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Huggingface发布从CC清理消重后的15T代币的中文网络数据FineWeb</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hhttps://huggingface.co/datasets/HuggingFaceFW/fineweb</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UER整理CLUECorpusSmall+新闻评论中英文</font></font></td>
<td><a href="https://github.com/dbiir/UER-py/wiki/%E9%A2%84%E8%AE%AD%E7%BB%83%E6%95%B0%E6%8D%AE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/dbiir/UER-py/wiki/%E9%A2%84%E8%AE%AD%E7%BB%83%E6%95%B0%E6%8D%AE</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">智源人工智能开源的wudao 200G预训练数据</font></font></td>
<td><a href="https://openi.pcl.ac.cn/BAAI/WuDao-Data" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/BAAI-WuDao/WuDaoMM</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">里屋社区开源资源收集中文互联网语料集MNBVC目标是对标ChatGPT的40T</font></font></td>
<td><a href="https://github.com/esbatmop/MNBVC"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/esbatmop/MNBVC</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复旦开源15万中文图书下载和抽取方案</font></font></td>
<td><a href="https://github.com/FudanNLPLAB/CBook-150K"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FudanNLPLAB/CBook-150K</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">书生万卷数据集来自公开网页多模态数据集，包括文本，图文和视频，其中文本1T，图文150G</font></font></td>
<td><a href="https://opendatalab.org.cn/OpenDataLab/WanJuan1_dot_0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opendatalab.org.cn/OpenDataLab/WanJuan1_dot_0</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">昆仑天工3.2TB中英语开源料</font></font></td>
<td><a href="https://github.com/SkyworkAI/Skywork"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/SkyworkAI/Skywork</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中文预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">浪潮开源的用于Yuan1.0训练的预训练中文料</font></font></td>
<td><a href="https://www.airyuan.cn/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.airyuan.cn/home</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">度小满开源60G金融预培训语料</font></font></td>
<td><a href="https://github.com/Duxiaoman-DI/XuanYuan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Duxiaoman-DI/XuanYuan</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第一个中文科学文献数据集CSL，也有多种NLP任务数据</font></font></td>
<td><a href="https://github.com/ydli-ai/CSL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ydli-ai/CSL</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平行语</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新闻评论中英平行语料，用于中英间知识迁移</font></font></td>
<td><a href="https://data.statmt.org/news-commentary/v15/training/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://data.statmt.org/news-commentary/v15/training/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多源数据集整合</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">opendatalab 整合了预训练阶段的多个数据源</font></font></td>
<td><a href="https://opendatalab.org.cn/?industry=9821&amp;source=JUU3JTlGJUE1JUU0JUI5JThF" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://opendatalab.org.cn/?industry=9821&amp;source=JUU3JTlGJUE1JUU0JUI5JThF</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具-搜索增强</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">webCPM开源和搜索工具进行交互问答的数据集，包括网页抽取式摘要，多事实内容回答等人工标签数据</font></font></td>
<td><a href="https://github.com/thunlp/WebCPM"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thunlp/WebCPM</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具-多工具</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BmTools 开源的多工具调用指令数据集</font></font></td>
<td><a href="https://github.com/OpenBMB/BMTools"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenBMB/BMTools</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具-多工具</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentInstruct包含6项Agent任务，包括REACT式COT标签</font></font></td>
<td><a href="https://thudm.github.io/AgentTuning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://thudm.github.io/AgentTuning/</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具-多工具</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MSAgent-Bench 大模型调用数据集 598k 训练数据</font></font></td>
<td><a href="https://modelscope.cn/datasets/damo/MSAgent-Bench/summary" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://modelscope.cn/datasets/damo/MSAgent-Bench/summary</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工具-多工具</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MOSS开源的知识搜索，文生图，外汇，解方程等4个插件的30万条多轮对话数据</font></font></td>
<td><a href="https://github.com/OpenLMLab/MOSS#%E6%95%B0%E6%8D%AE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/OpenLMLab/MOSS#%E6%95%B0%E6%8D%AE</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NL2SQL</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DB-GPT-Hub整理了多源text-to-sql数据集</font></font></td>
<td><a href="https://github.com/eosphoros-ai/DB-GPT-Hub"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/eosphoros-ai/DB-GPT-Hub</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">长文本</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">清华开源的长文本数据集LongAlign-10k</font></font></td>
<td><a href="https://huggingface.co/datasets/THUDM/LongAlign-10k" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://huggingface.co/datasets/THUDM/LongAlign-10k</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多模式-图表</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMC 图表理解问答数据集</font></font></td>
<td><a href="https://github.com/FuxiaoLiu/MMC"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/FuxiaoLiu/MMC</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国国际GC</font></font></h2><a id="user-content-aigc" class="anchor" aria-label="永久链接：AIGC" href="#aigc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">搜索</font></font></h3><a id="user-content-搜索" class="anchor" aria-label="永久链接：搜索" href="#搜索"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用搜索</font></font></h4><a id="user-content-通用搜索" class="anchor" aria-label="永久链接：通用搜索" href="#通用搜索"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://metaso.cn/about-us" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">秘塔搜索</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：融合了大脑图，表格多模态问答的搜索应用</font></font></li>
<li><a href="https://chrome.google.com/webstore/detail/youcom-ai-search-assistan/chamcglaoafmjphcfppikphgianmmbjf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You.COM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 支持多种搜索增强问答模式</font></font></li>
<li><a href="https://walles.ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Walles.AI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：融合了图像聊天、文本聊天、chatpdf、web-copilot等多种功能的智能助手</font></font></li>
<li><a href="https://www.webpilot.ai/signin?return-path=/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">webpilot.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">比ChatGPT自带的网页浏览更好用的浏览器搜索插件，更适用于复杂场景搜索，也开通api调用了</font></font></li>
<li><a href="https://www.bing.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新兵</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：需要科学上网哦</font></font></li>
<li><a href="https://www.perplexity.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Perplexity.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：同样需要科学上网，感觉比Bing做的更好的接入ChatGPT的神奇搜索引擎，在Bing之外还加入了相关推荐和追问</font></font></li>
<li><a href="https://sider.ai/download" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">sider.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 支持多模型浏览器插件对话和多模态交互操作</font></font></li>
<li><a href="https://so.360.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">360AI搜索</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">: 360的AI搜索和秘塔有些像</font></font></li>
<li><a href="https://mylens.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MyLens.AI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 支持时间轴，脑图等多种生成结果的搜索增强</font></font></li>
<li><a href="https://explorer.globe.engineer/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Globe Explorer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：搜索查询相关的知识并构建类似知识图谱的结构返回图片信息</font></font></li>
<li><a href="https://www.tiangong.cn/?channel=bing&amp;msclkid=3f5eebf016751939acb98dcd75d4dc02" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">天工AI搜&ZeroWidthSpace;&ZeroWidthSpace;索</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：和你相同的清晰模式搜索增强</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码搜索</font></font></h4><a id="user-content-代码搜索" class="anchor" aria-label="永久链接：代码搜索" href="#代码搜索"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://devv.ai/zh/search?threadId=d5kn5g4oz2m8" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">devv.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 基于架构llama2 + RAG架构的属于架构师的搜索引擎</font></font></li>
<li><a href="https://www.phind.com/?ref=allthingsai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phind</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 面向开发人员的 AI 引擎搜索</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知识管理</font></font></h4><a id="user-content-知识管理" class="anchor" aria-label="永久链接：知识管理" href="#知识管理"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://www.glean.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">收集</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：企业知识搜索和项目管理类的搜索公司，帮助员工快速定位信息，帮助公司整合信息</font></font></li>
<li><a href="https://get.mem.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mem</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 个人知识管理，例如知识图谱，已获openai融资</font></font></li>
<li><a href="https://github.com/BuilderIO/gpt-crawler"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT-Crawler</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：通过简单配置，可以自行提取网页的文本信息构建知识库，并进一步自定义GPT</font></font></li>
<li><a href="https://www.chatinsight.ai/?ref=aitoolnet.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatInsight</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：企业级文档管理，和基于文档的对话</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天文档</font></font></h3><a id="user-content-chatdoc" class="anchor" aria-label="永久链接：ChatDoc" href="#chatdoc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://kimi.moonshot.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kimi-Chat</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 长长长文档理解无敌的Kimi-Chat，单文档总结多文档格式对比，无所不能，多长都行！</font></font></li>
<li><a href="https://chatdoc.com/?viaurl=ainavpro.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatDoc</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :ChatPDF升级版，需要科学上网，增加了表格类解析，支持选择区域的问答，在PDF识别上做的很厉害</font></font></li>
<li><a href="https://askyourpdf.com/zh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AskyourPdf</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：同样是上传pdf进行问答和摘要的应用</font></font></li>
<li><a href="https://github.com/arc53/DocsGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DocsGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：比较早出来的Chat DOC通用方案</font></font></li>
<li><a href="https://chat2doc.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatPDF</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：国内的ChatPDF，上传pdf后，会给出文章的Top5可能问题，然后对话式从文档中进行问答和检索，10s读3万字</font></font></li>
<li><a href="https://www.alphabox.top/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlphaBox</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 从个人文件夹管理出发的文档问答工具</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文研究：日度更新，观点总结，</font></font></h3><a id="user-content-论文研究-日度更新观点总结" class="anchor" aria-label="永久链接：论文研究：日度更新，观点总结，" href="#论文研究-日度更新观点总结"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://typeset.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SCISPACE</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 论文研究的白月光，融合了全库搜索问答，以及个人上传PDF构建知识库问答。同样支持相关发现论文，和论文划词阅读。并且可以阅读内容保存到笔记本中方便后续查找，可以说是产品和算法强联合了。</font></font></li>
<li><a href="https://consensus.app/search/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">共识</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：AI加持的论文搜素，多论文总结，观点对比工具。产品巨高，但个人感觉搜索做的有提升空间</font></font></li>
<li><a href="https://www.aminer.cn/search/pub?q=Transformer&amp;t=b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Aminer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 论文搜索，摘要，问答，搜索关键词符号化改写；但论文知识库问答有些幻觉严重</font></font></li>
<li><a href="https://papers.cool/#" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cool.paper</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 苏神开发的基于kimi的论文阅读网站</font></font></li>
<li><a href="https://www.openread.academy/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenRead</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：国内产品，面向论文写作，阅读场景，可以帮助生成文献综述，以及提供和NotionAI相似的智能Markdown用于写作</font></font></li>
<li><a href="https://github.com/kaixindelele/ChatPaper"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatPaper</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 根据输入关键词，自动在arxiv上下载最新的论文，论文进行摘要总结，可以在huggingface上实验</font></font></li>
<li><a href="https://github.com/mukulpatnaik/researchgpt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Researchgpt</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 和ChatPDF类似，支持arivx论文下载，加载后对话式获取论文重点</font></font></li>
<li><a href="https://github.com/binary-husky/chatgpt_academic"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGPT-academic</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：又是一个基于gradio实现的paper润色， Abstract等功能资源的实现，功能明显可以抢先</font></font></li>
<li><a href="https://briefgpt.xyz/?viaurl=ainavpro.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BriefGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 日更Arxiv论文，论文论文进行摘要，关键词抽取，帮助一下了解最新动态，UI不错哟</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">写作效率工具类</font></font></h3><a id="user-content-写作效率工具类" class="anchor" aria-label="永久链接：写作效率工具类" href="#写作效率工具类"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://saibomaliang.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">赛博马良</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：题如其名，可定制AI员工24小时全网聚焦关注的创作选题，给个体小学进行二次创作</font></font></li>
<li><a href="https://www.yanmoai.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">研墨AI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：面向咨询领域的内容创作应用</font></font></li>
<li><a href="https://news.miracleplus.com/feeds?tab=hot" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Miracleplus</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：全AI代理负责运营的黑客新闻网站</font></font></li>
<li><a href="https://www.chatmind.tech/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatMind</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：chatgpt生成思维导图，模板很丰富，泛化性也不错，已经被XMind收购了</font></font></li>
<li><a href="https://ai.wolian.chat/openmao/#/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">范文喵写作</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：范文喵写作工具，选题，大纲，写作全流程</font></font></li>
<li><a href="https://app.writesonic.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WriteSonic</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：AI写作，支持对话和定向创作如广告文案，商品描述，支持网络搜索是亮点，支持中文</font></font></li>
<li><a href="https://www.copy.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">copy.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : WriteSonic竞品，亮点是像论文引用一样每句话都有对应网站链接，可以一键复制到右边的创作Markdown，超级好用！</font></font></li>
<li><a href="https://www.notion.so/product?fredir=1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NotionAI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：智能Markdown，适用真相！在创作中用命令调用AI辅助润色，扩写，搜索内容，给创意idea</font></font></li>
<li><a href="https://hix.ai/app/ai-writer/dashboard" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hix-AI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：同时提供副驾驶模式和综合写作模式</font></font></li>
<li><a href="https://ai-writer.com/?via=therundown" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI-Write</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：个人使用感觉较好的流程化写作工具</font></font></li>
<li><a href="https://www.jasper.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Jasper</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 同上，完全是竞品哈哈</font></font></li>
<li><a href="https://copyai.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">copy.down</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 中文的营销文案生成，只能定向定向，支持关键词到文案的生成</font></font></li>
<li><a href="https://www.aiwaves.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Weaver AI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：波形智能开发的内容创作应用程序，支持多场景写作</font></font></li>
<li><a href="https://chatexcel.com/convert" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatExcel</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 指令控制excel计算，对熟悉excel的一些鸡肋，对不熟悉的有点用</font></font></li>
<li><a href="https://www.mindshow.fun/#/folder/slides" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MindShow</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：免费+付费的PPT制作工具，自定义PPT模板还不够好</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融垂直领域</font></font></h3><a id="user-content-金融垂直领域" class="anchor" aria-label="永久链接：金融垂直领域" href="#金融垂直领域"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://reportify.cc/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportify</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 金融领域公司公告、新闻、电话会的问答和摘要总结</font></font></li>
<li><a href="https://alphalink-web.rabyte.cn/reading/home/my-focus" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alpha派</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">: kimi加持会议纪要 + 投研问答 + 各类金融资讯综合的一站式平台</font></font></li>
<li><a href="https://pro.fofinvesting.com/workbench/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">状况客FOF智能投顾</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：基金大模型应用，基金投顾，支持nl2sql类的数据查询，和基金信息对比查询等</font></font></li>
<li><a href="https://news.10jqka.com.cn/20240102/c653710580.shtml" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HithinkGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：同花顺发布金融大模型问财，覆盖查询、分析、对比、解读、预测等多个问题领域</font></font></li>
<li><a href="https://finchat.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinChat.io</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：使用最新的财务数据，电话会议记录，季度和年度报告，投资书籍等进行训练</font></font></li>
<li><a href="https://www.tigerbrokers.com.sg/market/gpt" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TigerGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 老虎证券，GPT4做个股分析，财报分析，投资知识问答</font></font></li>
<li><a href="https://chat.funddb.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatFund</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：韭圈儿发布的第一个基金大模型，看起来是做了多任务指令调整，并且APP现有的数据功能进行了全方位的打通，从选基，到持仓分析等等</font></font></li>
<li><a href="https://ai.0xscope.com/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ScopeChat</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :虚拟币应用，整个对话类似ChatLaw把工具组件嵌入了对话中</font></font></li>
<li><a href="https://www.ainvest.com/chat?ref=producthunt" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AInvest</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：个股投资，融合BI分析，讨论广场区（有演变成雪球热度指数的赶脚）</font></font></li>
<li><a href="https://www.transwarp.cn/product/infinity" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无涯Infinity</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :星环科技发布的金融大模型</font></font></li>
<li><a href="http://www.datagrand.com/products/aigc/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">曹植</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：达观发布金融大模型融合data2text等金融任务，赋能报告写作</font></font></li>
<li><a href="https://ai.eastmoney.com/welcome" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">妙想</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：东方财富自研金融大模型开放试用，但似乎申请一直未通过</font></font></li>
<li><a href="https://mp.weixin.qq.com/s/vLvxvi2nOywkjt7ppiFC2g" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">恒生LightGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :金融领域继续预训练+插件化设计</font></font></li>
<li><a href="https://www.ltxtrading.com/bondgpt" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">bondGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : GPT4在细分债券市场的应用开放申请中</font></font></li>
<li><a href="https://www.cnbc.com/2023/05/25/jpmorgan-develops-ai-investment-advisor.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IndexGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：摩根大通在研的生成式投资顾问</font></font></li>
<li><a href="https://public.com/alpha?ref=supertools.therundown.ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alpha</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : ChatGPT加持的金融app，支持个股信息查询，资产分析诊断，财报汇总等</font></font></li>
<li><a href="https://www.composer.trade/?ref=supertools.therundown.ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">作曲家</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：量化策略与AI的结合，聊天式+拖拽式投资组合构建与回测</font></font></li>
<li><a href="https://finalle.ai/?ref=supertools.therundown.ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Finalle.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：实时数据金融流接入大模型</font></font></li>
<li><a href="https://docs.openbb.co/terminal" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenBB</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：金融投资框架，</font></font><a href="https://openbb.co/blog/breaking-barriers-with-openbb-and-llamaIndex?utm_source=talkingdev.uwl.me" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenBB+LLamaIndex</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主要是大模型+API的使用方案，通过自然语言进行金融数据查询、分析和可视化</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">私人助理&amp;聊天</font></font></h3><a id="user-content-私人助理聊天" class="anchor" aria-label="永久链接：私人助理&amp;聊天" href="#私人助理聊天"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/JushBJJ/Mr.-Ranedeer-AI-Tutor"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mr.-Ranedeer-</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：基于prompt和GPT-4的强大能力提供个性化学习环境，个性化出题+模型解答</font></font></li>
<li><a href="https://www.ai-topia.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI Topiah</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 即时心智能角色AI聊天，和路飞唠了两句，多少有点中二之魂在燃烧</font></font></li>
<li><a href="https://www.chatbase.co/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天库</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">: 情感聊天，尚未尝试</font></font></li>
<li><a href="https://gptme.vana.com/login" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vana</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：虚拟DNA，通过聊天创造虚拟自己！概念很炫</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代理人</font></font></h3><a id="user-content-agent" class="anchor" aria-label="永久链接： 代理" href="#agent"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://nexus.snikpic.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NexusGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：AutoGPT 可以工作了，第一个全人工智能自由平台</font></font></li>
<li><a href="https://www.cognosys.ai/create" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cognosys</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 全网最火的web端AutoGPT，不过咋说呢实验了下感觉下巴要笑掉了，不剧去透试试你就知道了</font></font></li>
<li><a href="https://godmode.space/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">godmode</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：可以进行人为每一步交互的AutoGPT</font></font></li>
<li><a href="https://agentgpt.reworkd.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">agentgpt</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 基础版AutoGPT</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频拆条总结</font></font></h3><a id="user-content-视频拆条总结" class="anchor" aria-label="永久链接：视频拆条总结" href="#视频拆条总结"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://eightify.app/zh" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Evenify</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : chrome插件，节省观看长视频的时间，立即获取关键思想，分模块总结+时间摘要</font></font></li>
<li><a href="https://github.com/JimmyLv/BibiGPT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BibiGPT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : Bilibli视频内容一键总结，多模态文档</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码副驾驶和 BI 工具</font></font></h3><a id="user-content-代码copilot--bi工具" class="anchor" aria-label="永久链接：代码副驾驶和 BI 工具" href="#代码copilot--bi工具"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/OpenDevin/OpenDevin"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenDevin</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：CognitionAI发布再SWE-Bench上编码能力有显着提升的智能体</font></font></li>
<li><a href="https://www.codium.ai/products/alpha-codium/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlphaCodium</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：流程工程提高代码整体通过率</font></font></li>
<li><a href="https://ide.unitmesh.cc/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoDev</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : AI编程辅助工具</font></font></li>
<li><a href="https://www.codium.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Codium</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 开源的编程Copilot来啦</font></font></li>
<li><a href="https://github.com/features/copilot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">副驾驶</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">: 要付费哟</font></font></li>
<li><a href="https://github.com/fauxpilot/fauxpilot"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fauxpilot</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : copilot本地开源供应商</font></font></li>
<li><a href="https://codeium.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Codeium</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Copilot替代品，有免费版本支持各种插件！</font></font></li>
<li><a href="https://github.com/biobootloader/wolverine"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wolverine</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 代码调试的python脚本</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BI和DB工具</font></font></h3><a id="user-content-bi和db工具" class="anchor" aria-label="永久链接：BI和DB工具" href="#bi和db工具"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://tableagent.datacanvas.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TableAgent</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：九章云极推出的数据分析，机器学习智能体</font></font></li>
<li><a href="https://digitforce.com/contact/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SwiftAgent</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：数势科技推出的数据分析智能体</font></font></li>
<li><a href="https://cn.kyligence.io/copilot/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kyligence Copilot</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :Kyligence发布一站式指标平台的AI数智助手，支持对话式指标搜索，异动促销等等</font></font></li>
<li><a href="https://www.ai2sql.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ai2sql</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : text2sql老牌公司，相比sqltranslate功能更全面，支持SQL语法检查、清理和生成公式</font></font></li>
<li><a href="https://www.pingcap.com/chat2query-an-innovative-ai-powered-sql-generator-for-faster-insights/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">chat2query</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : text2sql 相比以上两者支持更自然的文本指令，以及更复杂的数据分析类的sql生成</font></font></li>
<li><a href="https://outerbase.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OuterBase</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : text2sql 设计风格很吸睛！电子表格结合mysql和dashboard，更适合数据分析宝宝</font></font></li>
<li><a href="https://github.com/chat2db/Chat2DB"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chat2DB</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：智能通用数据库SQL客户端和报表工具</font></font></li>
<li><a href="https://sf.163.com/about#event" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatBI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> :网易数帆发布ChatBI对话数据分析平台</font></font></li>
<li><a href="https://www.dataherald.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据先驱</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Text2SQL</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多模态生成</font></font></h3><a id="user-content-多模态生成" class="anchor" aria-label="永久链接：多模态生成" href="#多模态生成"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://beta.dreamstudio.ai/dream" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">dreamtudio.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 开创者，稳定扩散，有资源配额</font></font></li>
<li><a href="https://www.midjourney.com/home/?callbackUrl=%2Fapp%2F" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中途</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：开创者，艺术风格主导</font></font></li>
<li><a href="https://openai.com/product/dall-e-2" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dall.E</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 三方一起凑齐了</font></font></li>
<li><a href="https://huggingface.co/spaces/hysts/ControlNet" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ControlNet</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 为绘画创作加持可控性</font></font></li>
<li><a href="https://www.genmo.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gemo.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：多模态聊天机器人，包括文本，图像，视频生成</font></font></li>
<li><a href="https://storybird.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Storybird</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 根据提示词生成故事绘本，还可以售卖</font></font></li>
<li><a href="https://magnific.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Magnific.ai</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：两个人的团队进行AI图片精修</font></font></li>
<li><a href="https://app.morphstudio.com/waitlist" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Morph Studio</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : Stability AI 入场视频制作</font></font></li>
<li><a href="https://gamma.app/create/generate" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gamma</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : PPT制作神器，ProductHunt月度排名第1</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">资源</font></font></h2><a id="user-content-resources" class="anchor" aria-label="永久链接：资源" href="#resources"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTs应用导航</font></font></h3><a id="user-content-gpts应用导航" class="anchor" aria-label="永久链接：GPTs应用导航" href="#gpts应用导航"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://similargpts.com/store-ranks/sorted-by-total-chats" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">类似GPT：全网AI产品流量大全</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font></li>
<li><a href="https://www.gptseek.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPTSeek：大家投票最有价值的GPT应用</font></font></a></li>
<li><a href="https://www.producthunt.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ProductHunt：技术产品网站，各类热门AI技术产品的集散地</font></font></a></li>
<li><a href="https://github.com/dair-ai/AI-Product-Index"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI-产品索引</font></font></a></li>
<li><a href="https://github.com/TheExplainthis/AI-Products-All-In-One"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI-产品-一体机</font></font></a></li>
<li><a href="https://supertools.therundown.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TheRunDown: GPT 应用分类</font></font></a></li>
<li><a href="https://github.com/e2b-dev/awesome-ai-agents?tab=readme-ov-file"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超棒的AI代理：Agent应用收藏</font></font></a></li>
<li><a href="https://gpt4demo.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT 演示</font></font></a></li>
<li><a href="https://ai-bot.cn/#term-15" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI-Bot大众工具导航</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示和其他教程类</font></font></h3><a id="user-content-prompt和其他教程类" class="anchor" aria-label="永久链接：提示和其他教程类" href="#prompt和其他教程类"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/openai/openai-cookbook"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI Cookbook</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 提供OpenAI模型使用示例 ⭐</font></font></li>
<li><a href="https://promptperfect.jinaai.cn/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PromptPerfect</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：用魔法击败魔法，输入原始提示词，模型进行定向优化，实验后我有点沉默了，可以定向支持不同使用提示的模型如Difussion，ChatGPT，Dalle等</font></font></li>
<li><a href="https://www.clickprompt.org/zh-CN/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ClickPrompt</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 为各种提示加持的工具生成指令包括Difussion，chatgptdeng, 需要OpenAI Key</font></font></li>
<li><a href="https://newzone.top/chatgpt/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGPT ShortCut</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：提供各式场景下的提示示例，示例很全，使用后可以点赞！ ⭐</font></font></li>
<li><a href="https://enchanting-trader-463.notion.site/Full-ChatGPT-Prompts-Resources-8aa78bb226b7467ab59b70d2b27042e9" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完整的 ChatGPT 提示 + 资源</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：各种尝试的提示示例，以及更多场景有所不同</font></font></li>
<li><a href="https://learnprompting.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习提示</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：提示工程超全教程，和落地应用收藏，包括很多LLM调用Agent的高级场景⭐</font></font></li>
<li><a href="https://github.com/ORDINAND/The-Art-of-Asking-ChatGPT-for-High-Quality-Answers-A-complete-Guide-to-Prompt-Engineering-Technique"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向chatgpt询问高质量答案的艺术</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：如何写Prompt指令出书了，链接是中文翻译的版本，比较偏基础使用</font></font></li>
<li><a href="https://github.com/dair-ai/Prompt-Engineering-Guide"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prompt-Engineer-Guide</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 同学习提示类的集成教程，互相引用还可以吗？！分类索引做的更好一些 ⭐</font></font></li>
<li><a href="https://www.alignmentforum.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI Alignment Forum</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : RLHF 等相关最新论文和观点的讨论论坛</font></font></li>
<li><a href="https://www.deeplearning.ai/short-courses/langchain-chat-with-your-data/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Langchain：与你的数据对话</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：吴恩达LLM实践课程</font></font></li>
<li><a href="https://github.com/phodal/aigc"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构筑大语言模型应用：应用开发与架构设计</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：一本关于LLM的真实世界应用的开源电子书</font></font></li>
<li><a href="https://github.com/databricks-academy/large-language-models"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大语言模型：从生产到应用</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：大模型应用Edx出品的课程</font></font></li>
<li><a href="https://github.com/karpathy/minbpe"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Minbpe</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : Karpathy大佬撤回openai后整了个分词器的教学代码</font></font></li>
<li><a href="https://github.com/bbycroft/llm-viz"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM-VIZ</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 大模型结构可视化支持GPT系列</font></font></li>
<li><a href="https://baoyu.io/translations/prompt-engineering/how-i-won-singapores-gpt-4-prompt-engineering-competition" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我如何分解新加坡首届GPT-4工程提示大赛 [译]</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> : 干活很多的提示技巧</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">书籍和博客类</font></font></h3><a id="user-content-书籍和博客类" class="anchor" aria-label="永久链接：书籍和博客类" href="#书籍和博客类"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://openai.com/blog/chatgpt/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI ChatGPT 简介</font></font></a></li>
<li><a href="https://openai.com/blog/instruction-following/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI InstructGPT 简介</font></font></a></li>
<li><a href="https://yaofu.notion.site/How-does-GPT-Obtain-its-Ability-Tracing-Emergent-Abilities-of-Language-Models-to-their-Sources-b9a57ac0fcf74f30a1ab9e3e36fa1dc1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AllenAI ChatGPT能力解读：GPT如何获得能力？追踪语言模型的新兴能力的来源</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  ⭐</font></font></li>
<li><a href="https://huggingface.co/blog/dialog-agents" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Huggingface ChatGPT能力解读：ChatGPT背后的技术：RLHF、IFT、CoT、红队等</font></font></a></li>
<li><a href="https://writings.stephenwolfram.com/2023/02/what-is-chatgpt-doing-and-why-does-it-work/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stephen Wolfram ChatGPT能力解读：ChatGPT在做什么以及为什么有效？</font></font></a></li>
<li><a href="https://github.com/chenweiphd/ChatGPT-Hub"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chatgpt相关解读汇总</font></font></a></li>
<li><a href="https://www.jiqizhixin.com/articles/2018-11-15-6?from=timeline" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AGI历史与现状</font></font></a></li>
<li><a href="https://zhuanlan.zhihu.com/p/597586623" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">张俊林通向AGI之路：大型语言模型（LLM）技术精要</font></font></a></li>
<li><a href="https://www.zhihu.com/question/589639535/answer/2936696161" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知乎回答 OpenAI发布GPT-4，有哪些技术上的优化或突破？</font></font></a></li>
<li><a href="https://zhuanlan.zhihu.com/p/609877277" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">追赶ChatGPT的难点与平替</font></font></a></li>
<li><a href="https://zhuanlan.zhihu.com/p/615554635" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">压缩即泛化，泛化即智能</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  ⭐</font></font></li>
<li><a href="https://lilianweng.github.io/posts/2023-06-23-agent/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM 支持的自主代理</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  ⭐</font></font></li>
<li><a href="https://towardsdatascience.com/all-you-need-to-know-to-build-your-first-llm-app-eb982c78ffac" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建您的第一个 LLM 应用程序所需了解的所有内容</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  ⭐</font></font></li>
<li><a href="https://www.semianalysis.com/p/gpt-4-architecture-infrastructure" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT-4 架构、基础设施、训练数据集、成本、愿景、MoE</font></font></a></li>
<li><a href="https://weread.qq.com/web/bookDetail/93832630811e7e827g0173ca" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI 研究人员出书：为什么伟大不能被计划</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font></li>
<li><a href="https://mp.weixin.qq.com/s?__biz=MjM5ODY2OTQyNg==&amp;mid=2649769138&amp;idx=1&amp;sn=2c408b73f66a52e43ea991b957729519&amp;chksm=bec3d9af89b450b95e6432dc33f4f32ae7a29cc8e2916369aad6156c5817927d1f73a0c84e82&amp;scene=21#wechat_redirect" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拾象投研机构针对LLM的调研报告（文中有两个PPT的申请链接）</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font></li>
<li><a href="https://www.guotaixia.com/post/5336.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启明创投2023年生成式人工智能现状</font></font></a></li>
<li><a href="https://www.oneusefulthing.org/p/how-to-use-ai-to-do-stuff-an-opinionated" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何使用人工智能来做事：一个固执己见的指南</font></font></a></li>
<li><a href="https://www.interconnects.ai/p/llama-2-from-meta" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Llama 2：令人难以置信的开放式法学硕士</font></font></a></li>
<li><a href="https://book.douban.com/subject/36449803/?icn=index-latestbook-subject" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wolfram语言之父新书：这就是ChatGPT</font></font></a></li>
<li><a href="https://pair.withgoogle.com/explorables/grokking/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">谷歌出品：对大模型领悟能力的一些探索很有趣 机器学习模型是记忆还是泛化？</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ⭐</font></font></li>
<li><a href="https://yaofu.notion.site/An-Initial-Exploration-of-Theoretical-Support-for-Language-Model-Data-Engineering-Part-1-Pretraini-dc480d9bf7ff4659afd8c9fb738086eb" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">符尧大佬系列新作语言模型数据工程理论支持的初步探索。第 1 部分：预训练</font></font></a></li>
<li><a href="https://zhuanlan.zhihu.com/p/669015906" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奇绩创坛2023秋季路一览演日上创新LLM项目</font></font></a></li>
<li><a href="https://www.microsoft.com/en-us/research/blog/the-power-of-prompting/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示的力量微软首席科学家对提示在垂直领域使用的观点</font></font></a></li>
<li><a href="http://www.incompleteideas.net/IncIdeas/BitterLesson.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">惨痛教训强化学习之父总结AI研究的经验教训</font></font></a></li>
<li><a href="https://docs.google.com/presentation/d/1IWjo8bhoatWccCfGLYw_QhUI4zfF-MujN3ORIDCBIbc/edit?pli=1#slide=id.g2cbf00ff39c_0_173" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LlamaIndex：超越 RAG：构建高级上下文增强 LLM 应用程序</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">会议访谈类</font></font></h3><a id="user-content-会议访谈类" class="anchor" aria-label="永久链接：会议&amp;访谈类" href="#会议访谈类"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://www.technologyreview.com/2023/03/03/1069311/inside-story-oral-history-how-chatgpt-built-openai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工科技采访OpenAI工程师</font></font></a></li>
<li><a href="https://new.qq.com/rain/a/20230423A08J7400" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">陆奇最新演讲实录：我的大模型世界观｜第十四期</font></font></a></li>
<li><a href="https://simons.berkeley.edu/talks/ilya-sutskever-openai-2023-08-14" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI首席科学家最新讲座阅读LM无监督预训练了啥对泛化的观察</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></li>
<li><a href="https://www.mattprd.com/p/the-complete-beginners-guide-to-autonomous-agents" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自主代理完整初学者指南</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Octane AI 创始人 Matt Schlicht 发表的关于人工智能代理的一些思考</font></font></li>
<li><a href="https://mp.weixin.qq.com/s?__biz=MzA3MzI4MjgzMw==&amp;mid=2650893355&amp;idx=1&amp;sn=5911ccc05abf5177bb71a47ea5a748c8&amp;chksm=84e4a855b39321434a2a386c9f359979da99dd441e6cf8f88062f1e909ad8f5b9b24198d1edb&amp;scene=0&amp;xtrack=1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型（2023 年）</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> OpenAI 科学家最新大模型演讲</font></font></li>
<li><a href="https://www.youtube.com/watch?v=ahnGLM-RC1Y" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI闭门会议DevDay视频 - 最大化LLM表现技术的调查，无法翻墙可搜标题找笔记</font></font></a></li>
<li><a href="https://mp.weixin.qq.com/s/KWMvsvI85QI-GIXeXizDsA" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">月之暗面杨植麟专访,值得细读</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></li>
<li><a href="https://www.36kr.com/p/2716201666246790" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">吴恩达最新演讲：AI Agent工作流的未来</font></font></a></li>
<li><a href="https://fullstackdeeplearning.com/llm-bootcamp/spring-2023/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士训练营 2023</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件</font></font></h2><a id="user-content-papers" class="anchor" aria-label="永久链接：论文" href="#papers"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文列表</font></font></h3><a id="user-content-paper-list" class="anchor" aria-label="永久链接：论文列表" href="#paper-list"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/dongguanting/In-Context-Learning_PaperList"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/dongguanting/In-Context-Learning_PaperList</font></font></a></li>
<li><a href="https://github.com/thunlp/PromptPapers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thunlp/PromptPapers</font></font></a></li>
<li><a href="https://github.com/Timothyxxx/Chain-of-ThoughtsPapers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Timothyxxx&ZeroWidthSpace;&ZeroWidthSpace;/Chain-of-ThoughtsPapers</font></font></a></li>
<li><a href="https://github.com/thunlp/ToolLearningPapers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thunlp/ToolLearningPapers</font></font></a></li>
<li><a href="https://github.com/MLGroupJLU/LLM-eval-survey"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/MLGroupJLU/LLM-eval-survey</font></font></a></li>
<li><a href="https://github.com/thu-coai/PaperForONLG"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/thu-coai/PaperForONLG</font></font></a></li>
<li><a href="https://github.com/khuangaf/Awesome-Chart-Understanding"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/kuanaf/Awesome-Chart-Understanding</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">综述</font></font></h3><a id="user-content-综述" class="anchor" aria-label="永久链接：综述" href="#综述"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型综述</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预训练、提示和预测：自然语言处理中提示方法的系统调查 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自然语言处理的范式转变</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预训练模型：过去、现在和未来</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哪些语言模型架构和预训练对象最适合零样本泛化 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型中的推理：一项调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用语言模型提示进行推理：一项调查 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言模型概述：最新发展和展望 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型综述[6.29更新版]</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">统一大型语言模型和知识图：路线图</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">增强语言模型：调查 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域专业化是大型语言模型颠覆性的关键：一项综合调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的挑战和应用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于大型语言模型的代理的兴起和潜力：调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于信息检索的大型语言模型：调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人工智能调整：全面调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知识与大型语言模型集成的趋势：方法、基准和应用的调查和分类</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时间序列和时空数据的大型模型：调查与展望</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码语言模型调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型即服务 (MaaS)：调查</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大模型研究能力</font></font></h3><a id="user-content-大模型能力探究" class="anchor" aria-label="永久链接： 大模型能力研究" href="#大模型能力探究"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">情境学习
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更大的语言模型以不同的方式进行上下文学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">情境学习如何运作？理解与传统监督学习差异的框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 GPT 可以在上下文中学习？语言模型秘密执行梯度下降作为元优化器⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重新思考演示的作用 情境学习为何有效？ ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练有素的 Transformer 在上下文中学习线性模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">情境学习创建任务向量</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安慰能力
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用人工智能的火花：GPT-4 的早期实验</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的新兴能力 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代表空间和时间的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的新兴能力是海市蜃楼吗？</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">能力评估
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CHATGPT 是通用自然语言处理任务求解器吗？</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型可以从相关性推断因果关系吗？</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言模型的整体评价</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在实践中利用法学硕士的力量：对 ChatGPT 及其他内容的调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心理理论可能自发地出现在大型语言模型中</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超越模仿游戏：量化和推断语言模型的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型能解释自己吗？自然语言解释的反事实可模拟性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">揭秘代码生成的 GPT 自我修复</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序上训练的语言模型的意义证据</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解释对于校准黑盒模型有用吗</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于 ChatGPT 的鲁棒性：对抗性和非分布视角</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言习得：儿童和语言模型是否遵循相似的学习阶段？</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域能力
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT-4 解决医疗挑战问题的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用基础模型能否胜过专用调整？医学案例研究</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速调谐范式</font></font></h3><a id="user-content-prompt-tunning范式" class="anchor" aria-label="永久链接：提示调整范式" href="#prompt-tunning范式"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">免费调谐提示
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT2：语言模型是无监督的多任务学习者</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT3：语言模型是小样本学习者 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LAMA：语言模型作为知识库？</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动提示：从语言模型中获取知识</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复提示 LM 调整
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T5：使用统一的文本到文本转换器探索迁移学习的局限性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PET-TC(a)：利用完形填空问题进行少量文本分类和自然语言推理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PET-TC(b)：PETSGLUE 重要的不仅仅是大小，小语言模型也是小样本学习者</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GenPET：使用自然语言指令生成少量文本</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LM-BFF：让预训练的语言模型更好地帮助小样本学习者 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ADEPT：改进和简化模式利用训练</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fix-LM 提示调整
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前缀调优：优化生成的连续提示</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即时调整：规模的力量，可实现参数高效的即时调整 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">P-tunning：GPT 也理解 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WARP：字级对抗性重编程</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LM + 快速调谐
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">P-tunning v2：快速调整可以与跨尺度和任务的通用微调相媲美</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PTR：使用文本分类规则进行提示调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PADA：基于示例的即时学习，用于动态适应未见过的领域</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fix-LM 适配器调整
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LORA：大语言模型的低阶适配⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LST：用于参数和内存高效迁移学习的梯形侧调</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NLP 的参数高效迁移学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内在维度解释了语言模型微调的有效性</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主流LLMS和预训练</font></font></h3><a id="user-content-主流llms和预训练" class="anchor" aria-label="永久链接：主流LLMS和预训练" href="#主流llms和预训练"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GLM-130B：开放的双语预训练模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PaLM：通过路径扩展语言建模</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PaLM 2 技术报告</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT-4 技术报告</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">背包语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA：开放高效的基础语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Llama 2：开放基础和微调的聊天模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sheared LLaMA：通过结构化剪枝加速语言模型预训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenBA：从头开始预训练的开源 15B 双语非对称 seq2seq 模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">米斯特拉尔7B</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ziya2：以数据为中心的学习是法学硕士所需要的</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">巨型块：专家组合的高效稀疏训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TUTEL：大规模的自适应专家组合</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phi1- 教科书就是您所需要的 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phi1.5- 教科书就是你所需要的 II：phi-1.5 技术报告</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phi-3 技术报告：手机本地功能强大的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gemini：一系列高性能多模式模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上下文预训练：超越文档边界的语言建模</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLAMA PRO：具有块扩展功能的渐进式 LLaMA</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QWEN技术报告</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更少的截断改善了语言建模</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令参数&amp;对齐 (instruction_tunning)</font></font></h3><a id="user-content-指令微调对齐-instruction_tunning" class="anchor" aria-label="永久链接：指令配置&amp;调整 (instruction_tunning)" href="#指令微调对齐-instruction_tunning"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经典方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Flan：经过微调的语言模型是零学习者 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Flan-T5：扩展指令微调语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ExT5：迈向迁移学习的极限多任务扩展</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Instruct-GPT：训练语言模型以遵循人类反馈的指令 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T0：多任务提示训练实现零射击任务泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自然指令：通过自然语言众包指令进行跨任务泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tk-INSTRUCT：超自然指令：通过 1600 多个 NLP 任务的声明性指令进行泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ZeroPrompt：将基于提示的预训练扩展到 1,000 个任务，提高零样本泛化能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非自然指令：在（几乎）无需人工的情况下调整语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对指令调整的大型语言模型进行整体评估的指导</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SFT数据缩放定律
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LIMA：对齐少即是多 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">也许只需要0.5%的数据：低训练数据指令调优的初步探索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlpaGasus：用更少的数据训练更好的羊驼</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">instructionsGPT-4：用于微调 MiniGPT-4 的 200 条指令范式</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令挖掘：大型语言模型的高质量指令数据选择</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Polite Flamingo 进行视觉指令调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索指令数据扩展对大型语言模型的影响：对现实世界用例的实证研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用大型语言模型学习数学推理的尺度关系</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当扩展遇到 LLM 微调时：数据、模型和微调方法的效果</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新景观/景观方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WizardLM：使大型语言模型能够遵循复杂的指令⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">成为自我指导：引入早期停止标准以实现最小的指导调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过指令反向翻译进行自对准 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">混合专家与指令调整的结合：大型语言模型的成功组合</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Goat：经过微调的 LLaMA 在算术任务上优于 GPT-4</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PROMPT2MODEL：从自然语言指令生成可部署模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpinionGPT：对指令调整的法学硕士中的显性偏差进行建模</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过人工智能反馈的自我对弈和情境学习来改进语言模型协商</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过元学习神经网络进行类似人类的系统泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Magicoder：源代码就是您所需要的</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超越人类数据：利用语言模型扩展自我训练以解决问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成表征指令调优</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InsCL：一种数据高效的持续学习范式，用于通过指令微调大型语言模型</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令数据生成
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">APE：大型语言模型是人类水平的即时工程师 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自指令：使语言模型与自生成的指令保持一致 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iPrompt：通过可解释的自动提示用自然语言解释数据模式</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">翻转学习：猜指令！翻转学习使语言模型变得更强零样本学习者</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的公平引导的小样本提示</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令归纳：从几个例子到自然语言任务描述。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自我质量检查无监督知识引导对齐。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPT 自我监督，打造更好的数据注释器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Flan Collection 设计数据和方法</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自消耗生成模型变得疯狂</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructEval：指令选择方法的系统评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用微调数据覆盖预训练偏差</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用大型语言模型改进文本嵌入</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何降低通用能力损失
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有监督的微调数据组合如何影响大型语言模型的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">两阶段法学硕士微调，专业化程度较低，普遍化程度较高</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更多经验/实验报告
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BELLE：探索指令数据扩展对大型语言模型的影响：对现实世界用例的实证研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Baize：Baize：一种对自聊天数据进行参数高效调优的开源聊天模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型LM中文指令数据全参数与LoRA微调对比研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索ChatGPT的内容排名能力：与人类偏好一致性的初步研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">遵循汉语语言模型实现更好的教学：调查培训数据和评估的影响</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调企业法学硕士：实用指南和建议</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他的
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过多任务微调进行跨语言泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过自然语言众包指令进行跨任务泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UNIFIEDSKG：使用文本到文本语言模型统一和多任务结构化知识基础</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PromptSource：自然语言提示的集成开发环境和存储库</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ROLELLM：大型语言模型的基准测试、诱导和增强角色扮演能力</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对话模型</font></font></h3><a id="user-content-对话模型" class="anchor" aria-label="永久链接：对话模型" href="#对话模型"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LaMDA：对话应用程序的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sparrow：通过有针对性的人类判断来改善对话代理的一致性 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BlenderBot 3：一个部署的对话代理，不断学习负责任地参与</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何不评估你的对话系统：对话响应生成的无监督评估指标的实证研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DialogStudio：为对话式人工智能打造最丰富、最多样化的统一数据集集合</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过扩展高质量的教学对话来增强聊天语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DiagGPT：基于法学硕士的聊天机器人，具有面向任务对话的自动主题管理功能</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思维链 (prompt_chain_of_thought)</font></font></h3><a id="user-content-思维链-prompt_chain_of_thought" class="anchor" aria-label="永久链接：思维链 (prompt_chain_of_thought)" href="#思维链-prompt_chain_of_thought"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基础&amp;进阶方式
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[零样本-COT] 大型语言模型是零样本推理机 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[few-shot COT] 思维链提示引发大型语言模型推理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自洽改善了语言模型中的思维推理链</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从最少到最多的提示可以在大型语言模型中进行复杂的推理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思想之树：用大型语言模型有意识地解决问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">计划和解决提示：通过大型语言模型改进零样本思维链推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分解提示解决复杂任务的模块化方法</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连续提示分解复杂问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">验证和编辑：知识增强的思想链框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超越思维链，大型语言模型中的有效思维图推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">混合思维树：结合快慢思维进行多跳视觉推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LAMBADA：自然语言自动推理的后向链接</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思想算法：增强大型语言模型中思想的探索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思维图：用大型语言模型解决复杂的问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">渐进式提示提高了大型语言模型的推理能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型可以学习规则</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思维多样性提高大型语言模型的推理能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从复杂到简单：用小语言模型解开用于推理的认知树</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">退一步：通过大型语言模型中的抽象引发推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">作为优化器的大型语言模型</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分领域COT [数学、代码、表格、QA]
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用语言模型解决定量推理问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">展示您的作品：使用语言模型进行中间计算的暂存器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过基于过程和结果的反馈解决数学应用题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CodeRL：通过预训练模型和深度强化学习掌握代码生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T-SciQ：通过大型语言模型信号教授多模态思维链推理以进行科学问答</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习绩效提高代码编辑</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型是多功能分解器：分解证据和问题以进行基于表格的推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tab-CoT：零样本表格思想链</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码链：使用语言模型增强代码模拟器进行推理</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原理分析
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">理解思维链提示：对重要事项的实证研究 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文字和图案：为了形成有效的思维链，需要两个人去探戈</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">揭示思想链背后的奥秘：理论视角</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型很容易被不相关的上下文分散注意力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无提示的思维链推理</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">小模型COT尺寸
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将较小的语言模型专门用于多步推理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教授小语言模型进行推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型是推理老师</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将推理能力提炼成更小的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CoT Collection：通过思想链微调改进语言模型的零样本和少样本学习</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">COT样本自动构建/选择
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">STAR：自学推理机 Bootstrapping ReasoningWith Reasoning</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoCOT：大型语言模型中的自动思维提示链</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型可以自我改进</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的思想链主动提示</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于复杂性的多步推理提示</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他的
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OlaGPT 赋予法学硕士类似人类的解决问题的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有挑战性的 BIG-Bench 任务以及思维链能否解决它们</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型是具有自我验证功能的更好推理机</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ThoughtSource 大型语言模型推理数据的中心枢纽</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM 多步推理中的两个自洽失败</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLHF</font></font></h3><a id="user-content-rlhf" class="anchor" aria-label="永久链接：RLHF" href="#rlhf"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深脑
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教授语言模型以支持带有经过验证的引用的答案</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻雀，通过有针对性的人类判断改善对话代理的一致性⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">统计拒绝抽样提高偏好优化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言建模的强化自训练 (ReST)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SLiC-HF：利用人工反馈进行序列似然校准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">校准序列似然改善条件语言生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用语言模型进行奖励设计</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最终答案强化学习通过基于过程和结果的反馈解决数学应用题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过基于过程和结果的反馈解决数学应用题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超越人类数据：利用语言模型扩展自我训练以解决问题</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放性
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PPO：近端策略优化算法 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">符合人类偏好的深度强化学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据人类偏好微调语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习从人类反馈中总结</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructGPT：训练语言模型以遵循人类反馈的指令 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奖励模型的扩展法则过度优化 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从弱到强的泛化：通过弱监督激发强大的能力 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PRM：我们一步步验证一下</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">培训验证者解决数学应用题 [PRM 的依赖依赖]</font></font></li>
<li><a href="https://openai.com/blog/introducing-superalignment" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAI 超级对齐博客</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人择
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用语言助理作为对齐者的实验室</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">减少伤害的红队语言模型方法、扩展行为和经验教训</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过人类反馈的强化学习来训练一个有用且无害的助手⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来自人工智能反馈的宪法人工智能无害⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据人类偏好预训练语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型中的道德自我纠正能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">潜伏特工：通过安全培训持续培训欺骗性法学硕士</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AllenAI，RL4LM：强化学习（不是）用于自然语言处理基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">改良方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RRHF：对将语言模型与人类反馈保持一致的响应进行排名，不流泪</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">事后诸葛亮使语言模型与反馈保持一致</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlpacaFarm：从人类反馈中学习的方法的模拟框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAFT：生成基础模型对齐的奖励排名微调</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLAIF：通过人工智能反馈扩展人类反馈的强化学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在模拟人类社会中训练社会一致的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAIN：您的语言模型无需微调即可自我调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估一致性的生成法官</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深入了解偏好：揭开反馈获取的谜底，以调整大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">三文鱼：与遵循原则的奖励模式进行自我调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型忘却 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对抗偏好优化 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人类对齐的偏好排名优化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任重道远：研究 RLHF 中的长度相关性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使语言模型能够从数据中隐式学习自我改进</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奖励模型集合有助于缓解过度优化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从偏好中学习最佳优势，并将其误认为是奖励</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ULTRAFEEDBACK：通过高质量反馈增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主题：人工智能反馈的内在动机</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过优势模型和选择性排练稳定 RLHF</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Shepherd：语言模型生成的批评者</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习如何创造出比法学硕士更好的作品</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">细粒度的人类反馈为语言模型训练带来更好的回报</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从头开始以最少的人类监督实现语言模型的原理驱动的自我调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">直接偏好优化：你的语言模型实际上是一个奖励模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HIR 事后诸葛亮让语言模型成为更好的指令追随者</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Aligner：通过弱到强校正实现高效对准</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RL研究
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解 RLHF 对法学硕士普遍化和多样性的影响</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任重道远：研究 RLHF 中的长度相关性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奖励（不）一致性对 RLHF 的涓滴影响</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于人类反馈的强化学习的开放问题和基本限制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人类反馈不是黄金标准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据课程中训练后大型语言模型的对比</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM Agent让模型使用工具（llm_agent）</font></font></h3><a id="user-content-llm-agent-让模型使用工具-llm_agent" class="anchor" aria-label="永久链接：LLM代理让模型使用工具（llm_agent）" href="#llm-agent-让模型使用工具-llm_agent"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于大语言模型的自治代理综述</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">个人LLM代理：关于能力、效率和安全性的见解和调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于提示通用方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ReAct：在语言模型中协同推理和行动 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自问：测量和缩小语言模型中的组合性差距 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MRKL Systems一种模块化的神经符号架构，结合了大型语言模型、外部知识源和离散推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PAL：程序辅助语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ART：大型语言模型的自动多步推理和工具使用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ReWOO：将推理与观察解耦以实现高效的增强语言模型 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">针对知识密集型多步骤问题的交叉检索与思维链推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chameleon：使用大型语言模型进行即插即用的组合推理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">忠实的思维链推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反思：具有言语强化学习的语言智能体 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">验证和编辑：知识增强的思想链框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RestGPT：将大型语言模型与现实世界的 RESTful API 连接起来</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatCoT：基于聊天的大型语言模型的工具增强思想链推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructTODS：面向端到端任务的对话系统的大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TPTU：基于大型语言模型的人工智能代理的任务规划和工具使用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ControlLLM：通过搜索图来使用工具增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反射：具有动态记忆和自我反射的自主代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoAgents：自动代理生成框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GitAgent：通过工具扩展使用 GitHub 促进自治代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PreAct：在ReAct中预测未来增强Agent的规划能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TOOLLLM：促进大型语言模型掌握 16000 多个真实世界 API ⭐ -AnyTool：用于大规模 API 调用的自我反思、分层代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AIOS：LLM代理操作系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLMCompiler 用于并行函数调用的 LLM 编译器</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于USB的通用方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TALM：工具增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Toolformer：语言模型可以自学使用工具 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用基础模型进行工具学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tool Maker：大语言模型作为工具Maker</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TaskMatrix.AI：通过连接基础模型和数百万个API来完成任务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentTuning：为法学硕士启用通用代理能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SWIFTSAGE：一种针对复杂交互任务具有快速和慢速思维的生成代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FireAct：语言代理微调</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pangu-Agent：具有结构化推理的可微调多面手智能体</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST 与 REACT：多步推理的自我提升 LLM 代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过抽象链推理有效使用工具</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Agent-FLAN：大型语言模型有效代理调优的设计数据和方法</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentOhana：设计统一数据和训练管道以实现有效的代理学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Agent Lumos：开源语言代理的统一模块化培训</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调用模型方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HuggingGPT：使用 ChatGPT 及其 HuggingFace 中的朋友解决 AI 任务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gorilla：连接海量API的大型语言模型⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenAGI：当法学硕士遇到领域专家</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">垂直领域
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据分析
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DS-Agent：通过基于案例的推理增强大型语言模型的自动化数据科学</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InsightLens：在大语言模型支持的数据分析中从对话上下文中发现和探索见解</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据副驾驶：通过自主工作流程连接数十亿数据和人类</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InsightPilot 演示：法学硕士授权的自动化数据探索系统</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WeaverBird：通过大型语言模型、知识库和搜索引擎赋能财务决策</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinGPT：开源金融大语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinMem：具有分层内存和角色设计的性能增强型 LLM 交易代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AlphaFin：利用搜索增强股票链框架对财务分析进行基准测试</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融交易的多模式基础代理：工具增强、多样化和通才 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型能打败华尔街吗？揭示人工智能在选股方面的潜力</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生物医疗
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GeneGPT：使用领域工具增强大型语言模型，以改善对生物医学信息的访问</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChemCrow 使用化学工具增强大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过证据的期望最大化推理生成医学问答中的解释</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络网
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoWebGLM：引导和强化基于大型语言模型的 Web 导航代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有规划、长上下文理解和程序综合功能的真实世界 WebAgent</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mind2Web：迈向网络多面手代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用工作流引导探索在 Web 界面上进行 MiniWoB++ 强化学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WEBARENA：构建自治代理的现实网络环境</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AutoCrawler：用于生成网络爬虫的渐进式理解网络代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebLINX：具有多轮对话的真实世界网站导航</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebVoyager：使用大型多模式模型构建端到端 Web 代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CogAgent：GUI 代理的可视化语言模型</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebShop：与基础语言代理实现可扩展的现实世界网络交互</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ToolkenGPT：通过工具嵌入使用大量工具增强冻结的语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PointLLM：使大型语言模型能够理解点云</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用检索增强大语言模型进行可解释的长格式法律问答</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CarExpert：利用大型语言模型进行车内对话式问答</font></font></li>
</ul>
</li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">评估生成搜索引擎的可验证性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于在线决策的 Auto-GPT：基准和附加意见</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API-Bank：工具增强法学硕士的基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ToolLLM：促进大型语言模型掌握 16000 多个真实世界的 API</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过大型语言模型自动评估归因</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检索增强生成中大型语言模型的基准测试</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ARES：检索增强生成系统的自动评估框架</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多Agent
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成代理：人类行为的交互式模拟 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AgentVerse：促进多智能体协作并探索智能体的紧急行为</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAMEL：大规模语言模型社会“心灵”探索的交流代理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索交流游戏的大语言模型：《狼人杀》的实证研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">软件开发的通信代理 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元代理：通过协作生成代理模拟人类行为的交互，实现基于法学硕士的面向任务的协调</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">让模型说出密码：通过嵌入进行多智能体辩论</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MedAgents：作为零样本医学推理合作者的大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">战争与和平（WarAgent）：基于大型语言模型的世界大战多智能体模拟</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您只需要更多代理</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自主学习和探索的进化
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AppAgent：作为智能手机用户的多模式代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调查-巩固-利用：任务间智能体自我进化的通用策略</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Imaginarium 中的法学硕士：通过模拟试错来学习工具</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过行动学习增强大型语言模型代理的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">试错法：LLM 智能体基于探索的轨迹优化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OS-COPILOT：走向自我完善的通用计算机代理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼骑士：激发大型语言模型探索开放世界</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM+P：以最佳规划能力增强大型语言模型的能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">参考推理：大型语言模型的无损加速</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RecallM：时间上下文理解和问答的架构</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA Rider：激发大型语言模型探索开放世界</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士无法规划，但可以帮助在法学硕士模数框架中进行规划</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">抹布</font></font></h3><a id="user-content-rag" class="anchor" aria-label="永久链接：RAG" href="#rag"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebGPT：浏览器辅助问答与人工反馈</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebGLM：迈向具有人类偏好的高效网络增强问答系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebCPM：中文长篇问答互动网络搜索 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REPLUG：检索增强黑盒语言模型 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检索增强大型语言模型的查询重写</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RETA-LLM：检索增强型大型语言模型工具包</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Atlas：使用检索增强语言模型进行小样本学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RRAML：强化检索增强机器学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过检索增强研究大型语言模型的事实知识边界</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDFTriage：针对长结构化文档的问答</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自我反思：学习通过自我反省来检索、生成和批评 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">走过记忆迷宫：通过互动阅读超越语境限制⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示-搜索-预测：为知识密集型 NLP 构建检索和语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链中搜索：为知识密集型任务建立准确、可信、可追踪的大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主动检索增强生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">kNN-LM 无法改进开放式文本生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检索器增强语言模型可以推理吗？检索器和语言模型之间的指责游戏</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Query2doc：使用大型语言模型进行查询扩展 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RLCF：通过对比反馈将大型语言模型的能力与信息检索的上下文结合起来</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于自定义检索的增强嵌入</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DORIS-MAE：使用多级基于方面的查询进行科学文档检索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习过滤上下文以进行检索增强生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图上思考：知识图谱上大型语言模型的深度且负责任的推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RA-DIT：检索增强双指令调优</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过提示大型语言模型来扩展查询 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注释链：增强检索增强语言模型的鲁棒性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IAG：用于回答推理问题的归纳增强生成框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T2Ranking：大规模中文篇章排名标杆</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于开放式文本生成的事实增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FRESHLLMS：通过搜索引擎增强刷新大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KwaiAgents：具有大语言模型的通用信息查询代理系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">丰富的知识来源带来复杂的知识冲突：重新校准模型以反映相互矛盾的证据</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用野外检索的证据进行复杂的声明验证</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的检索增强生成：一项调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过迭代检索生成协同作用增强检索增强大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatQA：构建 GPT-4 级别的对话 QA 模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAG 与微调：管道、权衡和农业案例研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检索增强生成中大型语言模型的基准测试</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HyDE：无需相关标签的精确零样本密集检索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PROMPTAGATOR：从 8 个示例进行少量密集检索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">搜索与信息检索大语言模型之间的协同相互作用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">T-RAG：法学硕士的经验教训</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAT：检索增强思维在长视野生成中引发上下文感知推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ARAGOG：高级 RAG 输出分级</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ActiveRAG：通过主动学习揭示知识的宝藏</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提出正确的问题：通过强化学习进行主动问题重构 [传统方案参考]</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信息检索的查询扩展技术调查 [传统方案参考]</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习重写查询 [传统方案参考]</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理 Airbnb 搜索的多样性[传统方案参考]</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于召回和排名的新支持模型
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BGE M3-Embedding：通过自我知识蒸馏实现多语言、多功能、多粒度文本嵌入</font></font></li>
<li><a href="https://zhuanlan.zhihu.com/p/681370855" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网易为RAG设计的BCE嵌入技术报告</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BGE Landmark Embedding：一种用于检索增强长上下文大语言模型的无分块嵌入方法</font></font></li>
</ul>
</li>
<li><a href="https://contextual.ai/introducing-rag2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Contextual.ai-RAG2.0</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大模型图表理解和生成</font></font></h3><a id="user-content-大模型图表理解和生成" class="anchor" aria-label="永久链接：大模型图表理解和生成" href="#大模型图表理解和生成"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MMC：通过大规模指令调整推进多模态图表理解</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChartLlama：用于图表理解和生成的多模式法学硕士</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChartAssistant：通过图表到表格预训练和多任务指令调整的通用图表多模态语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChartInstruct：图表理解和推理的指令调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChartX 和 ChartVLM：复杂图表推理的多功能基准和基础模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MATCHA：通过数学推理和图表渲染增强视觉语言预训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UniChart：用于图表理解和推理的通用视觉语言预训练模型</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士+公斤级</font></font></h3><a id="user-content-llmkg" class="anchor" aria-label="永久链接：LLM+KG" href="#llmkg"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">综述类
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">统一大型语言模型和知识图：路线图</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型和知识图：机遇和挑战</font></font></li>
<li><a href="https://blog.csdn.net/m0_37586850/article/details/132463508" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知识图谱与大模型融合实践研究报告2023</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KG用于大模型推理
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用大型语言模型从知识图零样本自然语言生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MindMap：知识图谱提示在大型语言模型中激发思维图</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">知识增强语言模型提示零样本知识图问答</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用逻辑编程和大型语言模型对知识图进行特定领域问答</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自带 KG：零样本 KGQA 的自监督程序综合</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StructGPT：用于推理结构化数据的大型语言模型的通用框架</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于KG构建的大模型
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用大型语言模型增强知识图谱构建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法学硕士辅助知识图谱工程：ChatGPT 实验</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迭代零次法学硕士提示知识图构建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索大型语言模型以完成知识图谱</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人形特工</font></font></h3><a id="user-content-humanoid-agents" class="anchor" aria-label="永久链接：人形特工" href="#humanoid-agents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HABITAT 3.0：人类、化身和机器人的共居环境</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人形代理：模拟类人生成代理的平台</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Voyager：具有大型语言模型的开放式实体代理</font></font></li>
<li><a href="https://deepmind.google/discover/blog/shaping-the-future-of-advanced-robotics/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">塑造先进机器人技术的未来</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AUTORT：大规模机器人代理编排的具体基础模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过事后轨迹草图概括机器人任务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ALFWORLD：调整文本和实体环境以实现交互式学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MINEDOJO：利用互联网规模的知识构建开放式实体代理</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预训练数据(pretrain_data)</font></font></h3><a id="user-content-预训练数据pretrain_data" class="anchor" aria-label="永久链接：预训练数据(pretrain_data)" href="#预训练数据pretrain_data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DoReMi：优化数据混合加速语言模型预训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The Pile：用于语言建模的 800GB 不同文本数据集</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CCNet：从网络爬取数据中提取高质量的单语数据集</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">万卷：用于推进英文和中文大模型的综合多模态数据集</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CLUECorpus2020：用于预训练语言模型的大规模中文语料库</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上下文预训练：超越文档边界的语言建模</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据混合定律：通过预测语言建模性能来优化数据混合</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域模型SFT(domain_llms)</font></font></h3><a id="user-content-领域模型sftdomain_llms" class="anchor" aria-label="永久链接：领域模型SFT(domain_llms)" href="#领域模型sftdomain_llms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BloombergGPT：大型金融语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinVis-GPT：用于金融图表分析的多模态大语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CFGPT：大语言模型中文金融助手</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CFBenchmark：中国金融助手大语言模型基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InvestLM：使用金融领域指令调优的大型投资语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BBT-Fin：中文金融领域预训练语言模型、语料库和基准的全面构建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PIXIU：金融大语言模型、指令数据和评估基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinBen：大型语言模型的整体金融基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轩辕2.0：千亿参数的中国大型金融聊天模型</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生物医疗
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MedGPT：根据临床叙述预测医学概念</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BioGPT：用于生物医学文本生成和挖掘的生成式预训练变压器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PubMed GPT：生物医学文本的特定领域大语言模型 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatDoctor：利用医学领域知识在 LLaMA 模型上进行微调的医疗聊天模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Med-PaLM：大语言模型编码临床知识[V1,V2] ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SMILE：通过 ChatGPT 从单轮到多轮包容性语言扩展以提供心理健康支持</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">仲景：通过专家反馈和真实多轮对话提升中国医学大语言模型能力</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Galactia：大型科学语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有参数化知识指导的增强型大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatLaw 开源法律大语言模型 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MediaGPT：中文媒体大语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KITLM：将特定领域的知识集成到问答语言模型中</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EcomGPT：用于电子商务的具有任务链任务的指令调整大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TableGPT：将表、自然语言和命令统一到一个 GPT 中</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLEMMA：开放的数学语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MEDITAB：通过数据整合、丰富和细化扩展医学表格数据预测</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PLLaMa：植物科学的开源大型语言模型</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM超长文本处理 (long_input)</font></font></h3><a id="user-content-llm超长文本处理-long_input" class="anchor" aria-label="永久链接：LLM超长文本处理 (long_input)" href="#llm超长文本处理-long_input"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">位置编码、注意力机制优化
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Unlimiformer：具有无限长度输入的长距离变压器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型的并行上下文窗口</font></font></li>
<li><a href="https://spaces.ac.cn/archives/9617" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏剑林，NBCE：使用朴素贝叶斯扩展LLM的Context处理长度</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">结构化提示：将情境学习扩展到 1,000 个示例</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vcc：通过优先考虑重要代币，将 Transformer 扩展到 128K 代币或更多</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 RMT 将 Transformer 扩展到 100 万个代币甚至更多</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练短，测试长：线性偏差的注意力可以实现输入长度外推 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过位置插值扩展大型语言模型的上下文窗口</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LongNet：将 Transformer 扩展到 1,000,000,000 个代币</font></font></li>
<li><a href="https://kaiokendev.github.io/til#extending-context-to-8k" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://kaiokendev.github.io/til#extending-context-to-8k</font></font></a></li>
<li><a href="https://spaces.ac.cn/archives/9675" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏剑林,Transformer之路升级：10、RoPE是一种β射线编码</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></li>
<li><a href="https://spaces.ac.cn/archives/9706" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏剑林,变形金刚之路升级：11、将β射线位置进行到底</font></font></a></li>
<li><a href="https://spaces.ac.cn/archives/9708" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏剑林，变压器之路升级：12、无限外推的ReRoPE？</font></font></a></li>
<li><a href="https://spaces.ac.cn/archives/9859" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏剑林,变形金刚之路升级：15、归一化助力助力外推</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有注意力接收器的高效流语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Blockwise Transformer 实现近乎无限上下文的环注意力机制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YaRN：大型语言模型的高效上下文窗口扩展</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LM-INFINITE：大型语言模型的简单即时长度泛化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有注意力接收器的高效流语言模型</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高度压缩排序方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迷失在中间：语言模型如何使用长上下文 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLMLingua：&ZeroWidthSpace;&ZeroWidthSpace;压缩大型语言模型加速推理的提示</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LongLLMLingua：&ZeroWidthSpace;&ZeroWidthSpace;通过即时压缩在长上下文场景中加速和增强法学硕士 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习使用 Gist 标记压缩提示</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解锁法学硕士的上下文约束：通过基于自我信息的内容过滤提高法学硕士的上下文效率</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LongAgent：通过多代理协作将语言模型扩展到 128k 上下文</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PCToolkit：大语言模型统一即插即用提示压缩工具包</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练和模型架构方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">切勿从头开始训练：长序列模型的公平比较需要数据驱动的先验</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 4K 飙升至 400K：通过 Activation Beacon 扩展 LLM 的背景</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">永远不会迷失在中间：通过注意力强化问答来改进大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Focused Transformer：上下文缩放的对比训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基础模型的有效长上下文扩展</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论变形金刚的远距离能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高效的远程变压器：您需要更多关注，但不必关注每一层</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">姿势：通过位置跳跃训练有效扩展 LLMS 的上下文窗口</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LONGLORA：长上下文大型语言模型的高效微调</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LongAlign：大型语言模型长上下文对齐的秘诀</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将语言模型扩展到 128K 上下文的数据工程</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MEGALODON：具有无限上下文长度的高效 LLM 预训练和推理</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">效率优化
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高效注意力：具有线性复杂性的注意力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Transformer 是 RNN：具有线性注意力的快速自回归 Transformer</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HyperAttention：近线性时间的长上下文注意力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FlashAttention：具有 IO 感知功能的快速、内存高效的精确注意力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本越大，必要性越大：推理时间训练有助于长文本生成</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM长文本生成（long_output）</font></font></h3><a id="user-content-llm长文本生成long_output" class="anchor" aria-label="永久链接：LLM长文本生成（long_output）" href="#llm长文本生成long_output"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Re3：通过递归重新提示和修改生成更长的故事</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RECURRENTGPT：（任意）长文本的交互式生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DOC：通过详细的大纲控制提高长篇故事的连贯性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">韦弗：创意写作的基础模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用大型语言模型协助从头开始编写类似维基百科的文章</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NL2SQL</font></font></h3><a id="user-content-nl2sql" class="anchor" aria-label="永久链接：NL2SQL" href="#nl2sql"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大模型方案
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DIN-SQL：具有自校正功能的文本到 SQL 的分解上下文学习 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C3：使用 ChatGPT 进行零样本文本到 SQL ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL-PALM：改进的文本到 SQL 的大型语言模型适应</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BIRD LLM 可以作为数据库接口吗？用于大规模数据库的大型数据库基础文本到 SQL ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">跨域文本转 SQL 中自适应提示的基于案例的推理框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatDB：用数据库作为符号存储器来增强 LLMS</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGPT 零样本 Text-to-SQL 能力综合评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用结构和内容提示学习进行少量文本到 SQL 的翻译</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">领域知识密集型
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用公式化知识进行知识密集型文本到 SQL 语义解析</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过架构扩展弥补文本到 SQL 解析中的泛化差距</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">针对同义词替换的文本到 SQL 模型的鲁棒性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FinQA：财务数据数值推理数据集</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他的
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RESDSQL：解耦文本到 SQL 的模式链接和骨架解析</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MIGA：对话式文本到 SQL 的统一多任务生成框架</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码生成</font></font></h3><a id="user-content-code-generation" class="anchor" aria-label="永久链接：代码生成" href="#code-generation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AlphaCodium 生成代码：从即时工程到流程工程</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Codeforces 作为学习数字化编程的教育平台</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AlphaCode 生成竞赛级代码</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码链：通过具有代表性子模块的自我修订链实现模块化代码生成</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI 程序员就在我们中间：重新思考编程语言语法以实现高效代码生成</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">降低模型幻觉（可靠性）</font></font></h3><a id="user-content-降低模型幻觉-reliability" class="anchor" aria-label="永久链接：降低模型幻觉（可靠性）" href="#降低模型幻觉-reliability"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">民意调查
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型及其幻觉的危险</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自然语言生成中的幻觉调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI 海洋中的海妖之歌：大语言模型中的幻觉调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型基础模型中幻觉的调查</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大语言模型中的幻觉调查：原理、分类、挑战和开放问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">校准后的语言模型一定会产生幻觉</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 ChatGPT 无法提供真实答案？</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示或调整
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">R-Tuning：教授大型语言模型以拒绝未知问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">促使 GPT-3 变得可靠</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有任何问题都可以问我：提示语言模型的简单策略 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于让语言模型更好地推理的进展</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RefGPT：参考 → 由 GPT 和为 GPT 生成真实且定制的对话</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用检索重新思考：忠实的大语言模型推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成而不是检索：大型语言模型是强大的上下文生成器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型难以学习长尾知识</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解码策略
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相信你的证据：通过上下文感知解码减少幻觉⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自我完善：自我反馈迭代完善⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过自然语言推理增强预训练语言模型的自我一致性和性能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推理时间干预：从语言模型中得出真实答案</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启用大型语言模型来生成带引文的文本</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于开放式文本生成的事实增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KL-散度引导温度采样</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KCTS：具有令牌级幻觉检测的知识约束树搜索解码</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对比解码提高大型语言模型的推理能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对比解码：开放式文本生成作为优化</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探测与检测
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过大型语言模型自动评估归因</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QAFactEval：改进的基于 QA 的事实一致性评估的摘要</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大语言模型的零资源幻觉预防</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM 谎言：幻觉不是错误，而是作为对抗性示例的特征</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言模型（大多数）知道他们所知道的 ⭐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LM vs LM：通过交叉检查检测事实错误</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言模型知道它们何时出现幻觉引用吗？</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SELFCHECKGPT：生成大语言模型的零资源黑盒幻觉检测</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLMS 自相矛盾的幻觉：评估、检测和缓解</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放式世代的自我一致性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过多主体辩论提高语言模型的事实性和推理能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Selective-LAMA：语言模型置信感知评估的选择性预测</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM 可以表达他们的不确定性吗？法学硕士信心激发的实证评估</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检查和校准
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Truth-o-meter：与 llm 合作对抗幻觉</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RARR：使用语言模型研究和修改语言模型的内容</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">批评家：大型语言模型可以通过工具交互批评进行自我纠正</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 RELM 验证大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PURR：通过去噪语言模型损坏来有效编辑语言模型幻觉</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检查你的事实并再试一次：利用外部知识和自动反馈改进大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自适应变色龙或顽固树懒：揭示大型语言模型在知识冲突中的行为</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">啄木鸟：多模态大语言模型的幻觉校正</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">零样本忠实事实错误纠正</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大模型评估（evaluation）</font></font></h3><a id="user-content-大模型评估evaluation" class="anchor" aria-label="永久链接：大模型评估（evaluation）" href="#大模型评估evaluation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">事实性评估
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">值得信赖的 LLMS：评估大型语言模型一致性的调查和指南</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TrueTeacher：利用大型语言模型学习事实一致性评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TRUE：重新评估事实一致性评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FACTSCORE：长文本生成中事实精度的细粒度原子评估</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KoLA：仔细对大型语言模型的世界知识进行基准测试</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">何时不信任语言模型：研究参数和非参数记忆的有效性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FACTOOL：生成式人工智能中的事实检测，用于多任务和多领域场景的工具增强框架</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型中的长形式事实性</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检测任务
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从大型语言模型中检测预训练数据</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从（生产）语言模型中可扩展地提取训练数据</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用改写的样本重新思考语言模型的基准和污染</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推理优化（inference）</font></font></h3><a id="user-content-推理优化inference" class="anchor" aria-label="永久链接：推理优化（推理）" href="#推理优化inference"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速 Transformer 解码：一个写头即可满足您的需求</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过推测解码从 Transformer 进行快速推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GQA：从多头检查点训练通用多查询变压器模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思想骨架：大型语言模型可以进行并行解码</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SkipDecode：带有批处理和缓存的自回归跳过解码，可实现高效的 LLM 推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BatchPrompt：事半功倍</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型知识编辑黑科技(model_edit)</font></font></h3><a id="user-content-模型知识编辑黑科技model_edit" class="anchor" aria-label="永久链接：模型知识编辑黑科技(model_edit)" href="#模型知识编辑黑科技model_edit"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ROME：在 GPT 中查找和编辑事实关联</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Transformer 前馈层是键值存储器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MEMIT：在 Transformer 中批量编辑内存</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MEND：大规模快速模型编辑</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编辑大型语言模型：问题、方法和机遇</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言模型就是超级马里奥：从同源模型中吸收能力作为免费午餐</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型合并和剪枝(model_merge)</font></font></h3><a id="user-content-模型合并和剪枝model_merge" class="anchor" aria-label="永久链接：模型合并和剪枝(model_merge)" href="#模型合并和剪枝model_merge"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">混合就是您所需要的：万亿参数法学硕士的更便宜、更好的替代方案</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DARE 语言模型就是超级马里奥：从同源模型中吸收能力作为免费午餐</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用任务算术编辑模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TIES-Merging：解决合并模型时的干扰</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LM-Cocktail：通过模型合并对语言模型进行弹性调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SLICEGPT：通过删除行和列来压缩大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM Pretrainin 中通过贝叶斯优化进行检查点合并</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他提示工程师(prompt_engineer)</font></font></h3><a id="user-content-other-prompt-engineerprompt_engineer" class="anchor" aria-label="永久链接：其他提示工程师(prompt_engineer)" href="#other-prompt-engineerprompt_engineer"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用前校准：提高语言模型的小样本性能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">情境教学学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习绩效提高代码编辑</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过提示提高大型语言模型中的心理理论表现</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成知识提示进行常识推理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">背诵增强语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">kNN 提示：通过免校准的最近邻推理进行超越上下文的学习</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EmotionPrompt：利用心理学通过情感刺激增强大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于知识引导提示的因果关系概念提取</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">作为优化器的大型语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示作为程序：一种有效编译时提示优化的结构感知方法</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标记组提示在 GPT-4V 中释放非凡的视觉基础</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多式联运</font></font></h3><a id="user-content-multimodal" class="anchor" aria-label="永久链接：多式联运" href="#multimodal"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructBLIP：通过指令调整实现通用视觉语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Visual ChatGPT：使用 Visual Foundation 模型进行对话、绘图和编辑</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLava 视觉指令调整</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MiniGPT-4：利用先进的大语言模型增强视觉语言理解</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BLIVA：一个简单的多模式法学硕士，可以更好地处理文本丰富的视觉问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mPLUG-Owl：模块化赋予大型语言模型多模态能力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LVLM eHub：大型 VisionLanguage 模型的综合评估基准</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mirasol3B：用于时间对齐和上下文模态的多模态自回归模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PaLM-E：一种具体的多模态语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TabLLM：使用大型语言模型对表格数据进行少量分类</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AnyGPT：具有离散序列建模的统一多模态法学硕士</font></font></li>
<li><a href="https://openai.com/research/video-generation-models-as-world-simulators" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索拉技术报告</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迈向通用计算机控制：以 Red Dead Redemption II 的多模式代理为例</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">光学字符识别
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vary：扩大大型视觉语言模型的视觉词汇量</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型OCR模型：OCR缩放规律的实证研究</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型多模式模型中 OCR 隐藏的秘密</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PreFLMR：扩展细粒度后期交互多模态检索器</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时间序列法学硕士</font></font></h3><a id="user-content-timeseries-llm" class="anchor" aria-label="永久链接：Timeseries LLM" href="#timeseries-llm"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时间GPT-1</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时间序列和时空数据的大型模型：调查与展望</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TIME-LLM：通过重新编程大型语言模型进行时间序列预测</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大型语言模型是零样本时间序列预测器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TEMPO：用于时间序列预测的基于提示的生成预训练 Transformer</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于半导体制造中无监督故障检测的时间序列数据的生成预训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lag-Llama：走向时间序列预测的基础模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PromptCast：一种新的基于提示的时间序列预测学习范式</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">量化</font></font></h3><a id="user-content-quanization" class="anchor" aria-label="永久链接：量化" href="#quanization"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AWQ：LLM 压缩和加速的激活感知权重量化</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM-QAT：大型语言模型的无数据量化感知训练</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLM.int8() 大规模 Transformer 的 8 位矩阵乘法</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SmoothQuant 大型语言模型的准确高效的训练后量化</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对抗性攻击</font></font></h3><a id="user-content-adversarial-attacking" class="anchor" aria-label="永久链接：对抗性攻击" href="#adversarial-attacking"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">好奇心驱动的大型语言模型红队</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红队语言模型与语言模型</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索、建立、利用：从头开始红队语言模型</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他的</font></font></h3><a id="user-content-others" class="anchor" aria-label="永久链接：其他" href="#others"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你只需要在测试集上进行预训练哈哈作者你是懂讨论文学的</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习软件：小模型做大事</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成式人工智能的经济潜力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一个博士生对超大型语言模型时代 NLP 研究的看法</font></font></li>
</ul>
</article></div>
